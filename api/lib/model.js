'use strict';

module.exports = {

	load:(model)=>{

		let rtn = false;
		if(global._s.models.hasOwnProperty(model) === false){

			let loc = __dirname+'/../models/'+model+'.js';
			if (global._s.fs.existsSync(loc)) {

				global._s.models[model] = require(loc)();
				global._s._d.models.push({
					name:model,
					dt:(new Date()).getTime()
				});
				rtn = true;

			} else {

				rtn = 'Missing File.';
			
			};

		} else {

			let prop = global._s._d.models.findIndex(el=>{
			
				return el.name === model;
			
			});

			if(typeof prop === 'number'){
				
				global._s._d.models[prop].dt = (new Date()).getTime();
				rtn = true;
			
			} else {

				rtn = 'Missing Models Marker';
			
			}
			
		}
		return rtn;

	},
	clean:()=>{

	}

};