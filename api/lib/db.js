'use strict';

module.exports = {

	_:{

		connectionRetryInterval:500, // millisecons
		connectionRetries:10,
		
	},

	// Helper promise to connect to pool with retries
	_connect:function(counter){

		var that = this;
		return new Promise((resolve,reject)=> {

			try {
			
				if(!counter) {

					counter = 0;
					
				}
				
				if(that.pool) {

					that.pool.getConnection((err, connection)=>{

						if(err) {

							if(counter < that._.connectionRetries) {

								setTimeout(function(){

									that.getConnection(counter)
										.then(function(r){

											resolve(r);

										})
										.catch(function(e){

											reject(e);
											
										});

								},that._.connectionRetryInterval);
								
							} else {

								reject(err);

							}
							counter++;
						
						} else {

							resolve(connection);

						}

					});

				} else {

					reject('No Pool.');

				}

			} catch(e) {

				reject(e);

			}

		});

	},

	// Create Pool
	createPool:function(options){

		return new Promise((resolve,reject)=> {

			try {

				var mysql = require('mysql');
				var pool = mysql.createPool(options);

				resolve(pool);

			} catch(e) {

				reject(e);
			
			}

		});

	},

	// Execute MySQL query
	q:function(s,v){

		var that = this;
		return new Promise((resolve,reject)=> {

			try {

				that._connect()
					.then(connection=>{

						connection.query(s,v,(error,res)=>{

							connection.release();
							if(error) {
							
								reject(error);
							
							} else {

								resolve(res);

							}

						});	

					})
					.catch(e=>{

						reject(e);

					});

			} catch(e) {

				reject(e);

			}

		});

	}

};