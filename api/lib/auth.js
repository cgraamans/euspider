'use strict';

module.exports = {

	// jsonwebtoken decode
	decode:function(obj,secret){

		return new Promise((resolve,reject)=>{

			try {

				let d = global._s.crypto.jwt.verify(obj,secret);
				resolve(d);

			} catch(e) {

				reject(e);

			}

		});

	},

	// jsonwebtoken encode
	encode:function(obj,secret){

		return new Promise((resolve,reject)=>{

			try {

				let d = global._s.crypto.jwt.sign(obj,secret);
				resolve(d);

			} catch(e) {

				reject(e);

			}

		});

	},

	// emit with encode	
	emit:function(socket,payload,$state){

		var that = this;
		return new Promise((resolve,reject)=>{

			try {

				that.encode(payload,$state.secret)
					.then(emit=>{

						$state.socket.emit(socket,emit);
						resolve();

					})
					.catch(e=>{

						reject(e);

					});

			} catch(e) {

				reject(e);

			}

		});

	},

	// SHA256 encrypt
	encrypt:function(obj,isStr) {

		return new Promise((resolve,reject)=>{

			try {

				if(!isStr){
					obj = JSON.stringify(obj);
				}
				let d = global._s.crypto.SHA256(obj).toString();
				resolve(d);

			} catch(e) {

				reject(e);

			}

		});
			
	},

	check:{

		password:pwd=>{

			let owasp = require('owasp-password-strength-test');
			owasp.config({
			  allowPassphrases       : true,
			  maxLength              : 64,
			  minLength              : 10,
			  minPhraseLength        : 20,
			  minOptionalTestsToPass : 3,
			});
			
			return owasp.test(pwd);

		},
		name:name=>{

			return new Promise((resolve,reject)=>{

				global._s.db.q("SELECT id FROM users WHERE name = ?",[name])
					.then(hasName=>{

						if(hasName.length>0){

							reject();

						} else {

							resolve();
							
						}

					})
					.catch(e=>{

						reject(e);

					});

			});

		},
		email:uid=>{

			return new Promise((resolve,reject)=>{

				global._s.db.q("SELECT email FROM users WHERE id = ?",[uid])
					.then(hasEmail=>{

						if(hasEmail.length>0){

							if(hasEmail[0].email !== null){

								resolve(hasEmail[0].email);

							} else {

								reject();
							
							}

						} else {

							reject();
							
						}

					})
					.catch(e=>{

						reject(e);

					});

			});

		},

	},

	token:{

		verify:function(data){

			var that = this;
			return new Promise((resolve,reject)=>{

				let check = data;
				if(data.user) {

					check = data.user;

				}
				if(data._user) {

					check = data._user;

				}

				if(check) {

					if((!check.apiId) || (!check.token)) {

						resolve();

					} else {

						global._s.db.q('SELECT ub.reason, ub.dt FROM `user.bans` AS ub INNER JOIN users u ON u.id = ub.user_id WHERE ub.active = 1 AND u.api_id = ?',[check.apiId])
							.then(bans=>{

								if(bans.length>0){

									reject({bans:bans});
								
								} else {

									global._s.db.q('SELECT u.id,u.auth,u.name,ut.token,ut.dt,ut.persistent FROM users AS u INNER JOIN `user.tokens` ut ON ut.user_id = u.id WHERE u.api_id = ? AND ut.token = ? ORDER BY u.id DESC LIMIT 1',[
											check.apiId,
											check.token
										])
										.then(user=>{

											if(user.length > 0) {

												let u = user[0];
												let passBack = {

													u:{
														id:u.id,
														name:u.name,
														auth:u.auth,
													},
													payload:{
														apiId:check.apiId,
														token:u.token,
														name:u.name,
														auth:u.auth,
													}

												};

												if(u.dt > (Math.round((new Date()).getTime()/1000) - global._s.opt.auth.tokenTTL)) {

													resolve(passBack);

												} else {

													if(u.persistent < 1) {

														reject({action:'logout',m:['You have been forcefully logged out due to inactivity. Log in again.']})

													} else {

														that.create(u.id,1)
															.then(token=>{

																passBack.payload.token = token;
																resolve(passBack);

															})
															.catch(e=>{

																reject({e:e});

															});

													}

												}

											} else {

												reject({m:['Token verify: No token.']});

											}


										})
										.catch(e=>{

											reject({e:e});

										});

								}

							})
							.catch(e=>{

								reject({e:e});
							
							});

					}

				} else {

					resolve();

				}

			});

		},

		random:function(len,pos){

			var text = "";
			var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789!@#$%^&*()[]:;,?{}><+_-=";
	
			if(!len) {len = 64;}
			if(!pos) {pos = possible;}

			for (var i = 0; i < 64; i++)
				text += pos.charAt(Math.floor(Math.random() * pos.length));
			
			return text;

		},

		create:function(uid,persistent,len,pos){

			var that = this;
			return new Promise((resolve,reject)=>{

				if(typeof persistent === 'undefined') persistent = 1;
				if(persistent<1){
					persistent = 0;
				}
				let token = that.random(len,pos);

				let tIns = [

						uid,
						token,
						Math.round((new Date()).getTime() / 1000),
						persistent
						
				];

				global._s.db.q('INSERT INTO `user.tokens` (user_id,token,dt,persistent) VALUES (?)',[tIns])
					.then(()=>{

						resolve(token);

					})
					.catch(e=>{

						reject(e);

					});

			});		

		},

	},

	login:function(data){

		var that = this;
		return new Promise((resolve,reject)=>{

			let check = data,
				cont = false;
			if(data.user) {

				check = data.user;

			}
			if(data._user) {
				check = data._user;
			}

			// API Service Logins
			if(check.apiToken) {

				if(check.apiId) {

					cont = new Promise((res,rej)=>{

						global._s.db.q('SELECT ub.reason, ub.dt FROM `user.bans` AS ub INNER JOIN users u ON u.id = ub.user_id WHERE ub.active = 1 AND u.api_id = ?',[check.apiId])
							.then(bans=>{
								
								if(bans.length>0){

									rej({action:'banned',bans:bans,m:['You have been banned.']});

								} else {

									global._s.db.q('SELECT id, name, auth FROM users WHERE api_token = ? AND api_id = ? AND auth > 0 LIMIT 1',[check.apiToken,check.apiId])
										.then(user=>{
											if(user.length>0){

												res({u:{
													id:user[0].id,
													name:user[0].name,
													auth:user[0].auth,
												}});

											} else {

												rej({m:['User not found']});

											}
											
										})
										.catch(e=>{

											rej({e:e});

										});

								}

							})
							.catch(e=>{

								rej({e:e});

							});

					});

				}

			}

			// Initial user authentication by socket
			if(check.name){

				if(check.password) {

					cont = new Promise((res,rej)=>{

						global._s.db.q('SELECT ub.reason, ub.dt FROM `user.bans` AS ub INNER JOIN users u ON u.id = ub.user_id WHERE ub.active = 1 AND u.name = ?',[check.name])
							.then(bans=>{
								
								if(bans.length>0){

									rej({action:'banned',bans:bans,m:['You have been banned.']});

								} else {

									that.encrypt(check.password,true)
										.then(pwd=>{

											global._s.db.q('SELECT id, auth, api_id FROM users WHERE name = ? AND password = ? LIMIT 1',[check.name,pwd])
												.then(user=>{

													if(user.length>0){

														that.token.create(user[0].id,check.persistent)
															.then(token=>{

																	let send = {
																		u:{
																			id:user[0].id,
																			name:check.name,
																			auth:user[0].auth,
																		},
																		payload:{
																			apiId:user[0].api_id,
																			token:token,
																			name:check.name,
																			auth:user[0].auth,																			
																		}
																	};
																	res(send);

																})
																.catch(e=>{

																	rej({e:e});

																});

													} else {

														rej({m:['User not found']});

													}
													
												})
												.catch(e=>{

													rej({e:e});

												});

										})
										.catch(e=>{

											rej({e:e});

										});

								}

							})
							.catch(e=>{

								rej({e:e});

							});

					});

				}

			}

			// Initial user authentication by socket on return
			if(check.token){

				if(check.apiId) {

					cont = that.token.verify(data);
							
				}

			}

			if(cont !== false) {

				cont.then(verifiedUser=>{

						resolve(verifiedUser);

					})
					.catch(e=>{

						reject(e);

					});

			} else {

				reject({m:['Missing identifiers']});

			}

		});

	},

	bans:function(host){

		var that = this;
		return new Promise((resolve,reject)=>{

			try {

				global._s.db.q('SELECT reason, dt FROM `user.bans` WHERE active = 1 AND ((? LIKE CONCAT("%",host,"%")) OR (? LIKE CONCAT(host,"%")) OR (? LIKE CONCAT("%",host)))',[
						host,
						host,
						host
					])
					.then(result=>{
						
						if(result.length>0){

							resolve(result);

						} else {

							resolve();

						}

					})
					.catch(e=>{

						reject(e);
					
					});

			} catch(e) {

				reject(e);

			}

		});

	}

};