'use strict';

module.exports = ()=>{

	let obj = {

		sql:{

			get:{
				
				anon:'SELECT ni.id as stub, ni.title, ni.link, ni.author, ni.summary, ni.description, ni.pubDate, ni.img, ni._dt, ns.url_twitter as source_url_twitter, ns.url_rss as source_url_rss, ns.type as source_type, ns.name as source_name, ns.stub as source_stub, ns.logo as source_logo, ns.country as source_country, ns.language as source_language, ( COALESCE(l.votes,0)  - COALESCE(dl.votes,0)) AS votes, COALESCE(c.commented,0) AS comments FROM `news.items` as ni INNER JOIN `news.sources` ns ON ns.id = ni._source_id LEFT JOIN (SELECT COUNT(*) AS votes, item_id FROM `user.voted.items` WHERE dir = 1 GROUP BY item_id) AS l ON l.item_id = ni.id LEFT JOIN (SELECT COUNT(*) AS votes, item_id FROM `user.voted.items` WHERE dir < 0 GROUP BY item_id) AS dl ON dl.item_id = ni.id LEFT JOIN (SELECT COUNT(*) AS commented, item_id FROM `news.comments` WHERE isRemoved = 0 AND isDeleted = 0 GROUP BY item_id) AS c ON c.item_id = ni.id',
				user:'SELECT ni.id as stub, ni.title, ni.link, ni.author, ni.summary, ni.description, ni.pubDate, ni.img, ni._dt, ns.name as source_name, ns.type as source_type, ns.url_twitter as source_url_twitter, ns.url_rss as source_url_rss, ns.stub as source_stub, ns.logo as source_logo, ns.country as source_country, ns.language as source_language, ( COALESCE(l.votes,0)  - COALESCE(dl.votes,0)) AS votes, COALESCE(c.commented,0) AS comments, COALESCE(nsvi.dir,0) AS vote, COALESCE(nsi.active,0) AS saved FROM `news.items` as ni INNER JOIN `news.sources` ns ON ns.id = ni._source_id LEFT JOIN (SELECT COUNT(*) AS votes, item_id FROM `user.voted.items` WHERE dir = 1 GROUP BY item_id) AS l ON l.item_id = ni.id LEFT JOIN (SELECT COUNT(*) AS votes, item_id FROM `user.voted.items` WHERE dir < 0 GROUP BY item_id) AS dl ON dl.item_id = ni.id LEFT JOIN (SELECT COUNT(*) AS commented, item_id FROM `news.comments` WHERE isRemoved = 0 AND isDeleted = 0 GROUP BY item_id) AS c ON c.item_id = ni.id LEFT JOIN `user.voted.items` nsvi ON nsvi.item_id = ni.id AND nsvi.user_id = ? LEFT JOIN `user.saved.items` nsi ON nsi.item_id = ni.id AND nsi.user_id = ?',
				count:'SELECT COUNT(ni.id) AS r FROM `news.items` as ni INNER JOIN `news.sources` ns ON ns.id = ni._source_id',

			},

			conditionals:{

				country:'ns.country = ?',
				language:'ns.language = ?',

				from:'ni.id < ?',
				
				source:'ns.stub = ?',
				byId:'ni.id = ?',

				dt:'ni._dt > ?',
				type:'ns.type = ?',

				savedBy:'ni.id IN (SELECT item_id FROM `user.saved.items` WHERE user_id = ? AND active = 1)',
				sourced:'ns.id IN (SELECT source_id FROM `user.saved.sources` WHERE user_id = ? AND active = 1)',
				
				isRSS:'ns.url_rss IS NOT NULL',
				isTwitter:'ns.url_twitter IS NOT NULL',

			},

			orderBy:{

				dtUp:'ORDER BY ni._dt DESC',
				dtDown:'ORDER BY ni._dt ASC',

				votesUp:'ORDER BY votes DESC, ni._dt DESC',
				votesDown:'ORDER BY votes ASC, ni._dt DESC',
				commentsUp:'ORDER BY comments DESC, ni._dt DESC',

			},

			limit: ' LIMIT ?',

		},

		fn:{

			conditionalize:(conditionals,data)=>{

				let sqlArr = [];
				for (var prop in obj.sql.conditionals) {

					let preamble = 'AND';
					if(data[prop]){

						if(sqlArr.length === 0) {

							preamble = ' WHERE';

						}

						sqlArr.push(preamble);
						sqlArr.push(obj.sql.conditionals[prop]);
						
						if(data[prop] !== 'NULL'){
						
							conditionals.push(data[prop]);	
						
						}
						
					}

				}
				return {
					conditionals:conditionals,
					sql:sqlArr.join(' ')
				};

			},

			order:(data)=>{

				let sql = ' '+obj.sql.orderBy.dtUp;
				if(data.orderBy){

					sql = ' '+obj.sql.orderBy[data.orderBy];

				}
				return sql;

			},

			unstubbify:(stub)=>{

				let rtn = false;
				if(/^[a-zA-Z0-9]+$/.test(stub)){
						
					let id = global._s.crypto.base62.decode(stub);
					if(id){

						rtn = id;

					}

				}
				return rtn;

			},

		},

		clean:(items)=>{

			for(var i=0,c=items.length;i<c;i++){

				items[i].stub = global._s.crypto.base62.encode(items[i].stub);

			}
			return items;

		},

		verify:(data,options)=>{

			let rData = {
				ok:true
			};
			if(data.from){

				let fromId = obj.fn.unstubbify(data.from);
				if(fromId){

					rData.from = fromId;

				} else {

					rData.ok = false;
					rData.err = 'Bad from';

				}

			}
			
			if(data.language){

				rData.language = data.language;

			}

			if(data.item){

				let singleId = obj.fn.unstubbify(data.item);
				if(singleId){

					rData.byId = singleId;
				
				} else {
				
					rData.ok = false;
					rData.err = 'Bad item';

				}

			}

			if(data.time) {
					
				let now = Math.round((new Date()).getTime()/1000);
				if(typeof data.time === 'number'){

					if(data.time > 2592000){

						rData.dt = now - 2592000;

					} else {

						rData.dt = now - data.time

					}

				} else {

					rData.dt = now - 2592000;
					
				}

			}

			if(data.savedBy){

				if(options.userName){

					if(data.savedBy === options.userName) {

						rData.savedBy = options.userId;

					} else {

						rData.ok = false;
						rData.err = 'You are not authorized.';

					}

				} else {

					rData.ok = false;
					rData.err = 'You are not authorized.';

				}

			}

			if(data.sourced){

				if(options.userName){

					if(data.sourced === options.userName) {

						rData.sourced = options.userId;

					} else {

						rData.ok = false;
						rData.err = 'You are not authorized.';

					}

				} else {

					rData.ok = false;
					rData.err = 'You are not authorized.';

				}

			}

			if(data.up){

				if(typeof data.up === 'number') {

					if(data.up.toString().length === 10){

						rData.dt = data.up;

					}

				}

			}

			if(data.country) {

				data.country = data.country.toLowerCase();
				if(data.country.length !== 2) {
					
					rData.country = 'us';

				} else {
					
					rData.country = data.country;
				
				}

			}

			if(data.isRss){

				if(data.isRss === 1){

					rData.isRss = 'NULL';

				}

			}

			if(data.isTwitter){

				if(data.isRss === 1){

					rData.isTwitter = 'NULL';

				}

			}

			if(data.source){

				rData.source = data.source;

			}

			if(data.category){

				switch(data.category) {

					case 'likes':
						data.orderBy = 'votesUp';
						break;
					case 'comments':
						data.orderBy = 'commentsUp';
						break;
					case 'dislikes':
						data.orderBy = 'votesDown';
						break;
					case 'new':
						data.orderBy = 'dtUp';
						break;
					case 'old':
						data.orderBy = 'dtDown';
						break;

				}

			}

			if(data.orderBy){

				if(data.orderBy in obj.sql.orderBy){

					rData.orderBy = data.orderBy;

				}

			}

			if(data.user){

				rData.user = data.user;

			}

			if(data.sourceType) {

				rData.type = data.sourceType;

			}

			return rData;

		},

		count:(data)=>{

			return new Promise((resolve,reject)=>{

				let conditionals = [],
					sql = obj.sql.get.count;

				let conditionalObj = obj.fn.conditionalize(conditionals,data);
				if(conditionalObj){
					if((conditionalObj.conditionals) && (conditionalObj.sql)){

						sql += conditionalObj.sql;
						conditionals = conditionalObj.conditionals;
					
					}
				}

				global._s.db.q(sql,conditionals)
					.then(rowCounter=>{

						resolve(rowCounter[0].r);
					
					})
					.catch(e=>{
					
						reject(e);
					
					});

			});

		},

		get:(data,options)=>{

			return new Promise((resolve,reject)=>{

				if(!options){
					options={};
				}

				// Maximum rows
				if(!options.limit){
					options.limit = 50;
				}

				let conditionals = [],
					sql = obj.sql.get.anon;

				if(options.userId) {

					conditionals.push(options.userId);
					conditionals.push(options.userId);

					sql = obj.sql.get.user;
				
				}

				let conditionalObj = obj.fn.conditionalize(conditionals,data);
				if(conditionalObj){
					if((conditionalObj.conditionals) && (conditionalObj.sql)){

						sql += conditionalObj.sql;
						conditionals = conditionalObj.conditionals;
					
					}
				}

				// console.log(conditionals);


				let order = obj.fn.order(data);
				if(order){
					sql += order;
				}
				
				sql += obj.sql.limit;
				conditionals.push(options.limit);

				// console.log(sql);
				// console.log(conditionals);

				global._s.db.q(sql,conditionals)
					.then(items=>{

						resolve(items);
					
					})
					.catch(e=>{
					
						reject(e);
					
					});

			});

		}

	}

	return obj;

};