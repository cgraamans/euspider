'use strict';

module.exports = ()=>{

	let obj = {

		sql:{

			get:{
				
				// general population query
				anon:'SELECT nc.id, nc.item_id, nc.parent_id, nc.txt, nc.dt, nc.dt_edit, nc.isDeleted, nc.isRemoved, COALESCE(nurc.reported,0) as reported, (COALESCE(voteUp.vote,0) - COALESCE(voteDown.vote,0)) AS votes , u.name as user_name FROM `news.comments` AS nc INNER JOIN `users` u ON u.id = nc.user_id LEFT JOIN (SELECT count(*) AS vote, comment_id FROM `user.voted.comments` WHERE dir > 0 GROUP BY comment_id) voteUp ON voteUp.comment_id = nc.id LEFT JOIN (SELECT count(*) AS vote, comment_id FROM `user.voted.comments` WHERE dir < 0 GROUP BY comment_id) voteDown ON voteDown.comment_id = nc.id LEFT JOIN (SELECT COUNT(*) AS reported, comment_id FROM `news.comment.reported` GROUP BY comment_id) AS nurc ON nurc.comment_id = nc.id',

				// user query; pass user id 2ce plz
				user:'SELECT nc.id, nc.item_id, nc.parent_id, nc.txt, nc.dt, nc.dt_edit, nc.isDeleted, nc.isRemoved, COALESCE(nurc.reported,0) as reported, (COALESCE(voteUp.vote,0) - COALESCE(voteDown.vote,0)) AS votes , u.name as user_name, COALESCE(saved.active,0) as isSaved, COALESCE(nuvc.dir,0) AS isVotedOn, u.id as uid FROM `news.comments` AS nc INNER JOIN `users` u ON u.id = nc.user_id LEFT JOIN (SELECT count(*) AS vote, comment_id FROM `user.voted.comments` WHERE dir > 0 GROUP BY comment_id) voteUp ON voteUp.comment_id = nc.id LEFT JOIN (SELECT count(*) AS vote, comment_id FROM `user.voted.comments` WHERE dir < 0 GROUP BY comment_id) voteDown ON voteDown.comment_id = nc.id LEFT JOIN (SELECT COUNT(*) AS reported, comment_id FROM `news.comment.reported` GROUP BY comment_id) AS nurc ON nurc.comment_id = nc.id LEFT JOIN `user.saved.comments` saved ON saved.comment_id = nc.id AND saved.user_id = ? LEFT JOIN `user.voted.comments` nuvc ON nuvc.comment_id = nc.id AND nuvc.user_id = ?',
				
				// recursive reply query
				repliesTo:'SELECT COUNT(*) as repliesTo FROM (SELECT nc.id FROM `news.comments` AS nc WHERE nc.id IN( SELECT id FROM ( SELECT parent_id, id FROM `news.comments`) base,(SELECT @pv := ?) tmp WHERE find_in_set(parent_id, @pv) > 0 AND @pv := concat(@pv, ",", id) ) ) AS nc_store',
				
				// number of total rows (conditionalize separately)
				rows:'SELECT COUNT(*) as numrows FROM `news.comments` AS nc INNER JOIN users u ON u.id = nc.user_id'

			},

			param:{
					
				item:'nc.item_id = ?',
				top:'nc.parent_id IS NULL',
				parent:'nc.parent_id = ?',
				from:'nc.id < ?',
				upTo:'nc.id > ?',
				by:'u.name = ?',
				saved:'nc.id IN (SELECT comment_id FROM `user.saved.comments` WHERE user_id = ? AND active = 1)',
				comment:'nc.id = ?',

			},

			orderBy:{

				votesUp:'ORDER BY votes DESC',
				votesDown:'ORDER BY votes ASC',
				idUp:'ORDER BY nc.id DESC',
				idDown:'ORDER BY nc.id ASC',

			},

		},

		fn:{

			//  the stub => the id
			unstubbify:(stub)=>{

				let rtn = false;
				if(/^[a-zA-Z0-9]+$/.test(stub)){
						
					let id = global._s.crypto.base62.decode(stub);
					if(id){

						rtn = id;

					}

				}
				return rtn;

			},

			// set ordering
			order:(data)=>{

				let sql = obj.sql.orderBy.idUp;
				if(data.orderBy){

					if(obj.sql.orderBy[data.orderBy]) {

						sql = obj.sql.orderBy[data.orderBy];

					}

				}
				return sql;

			},

			// clean output of get
			clean:(data,options)=>{

				for(let i=0,c=data.length;i<c;i++) {

					// stub
					data[i].stub = global._s.crypto.base62.encode(data[i].id);
					delete data[i].id;
					
					data[i].item = global._s.crypto.base62.encode(data[i].item_id);
					delete data[i].item_id;

					//parent
					if(data[i].parent_id !== null) {
						data[i].parent = global._s.crypto.base62.encode(data[i].parent_id);	
					} else {
						data[i].parent = null;
					}
					delete data[i].parent_id;
					
					if((data[i].isRemoved===1) || (data[i].isDeleted===1)) {
						data[i].txt = '';
					}

					// uid
					if(options.userId){
						
						data[i].isByUser = 0;
						if(data[i].uid === options.userId){

							data[i].isByUser = 1;

						}

					}
					delete data[i].uid;



				}
				return data;

			},

			// verify input data
			verify:(data,options)=>{

				let rData = {};

				if(data.comment) {

					let commentId = obj.fn.unstubbify(data.comment);
					if(commentId){
						rData.comment = commentId;
					}

				}

				if(data.by) {

					if(data.saved){

						if (data.by === options.userName){

							rData.saved = options.userId;

						} else {

							rData.by = data.by;

						}

					} else {

						rData.by = data.by;	
					
					}
					 
				}

				if(data.item){


					let itemId = global._s.models.comments.fn.unstubbify(data.item);
					if(itemId){

						rData.item = itemId;

					}

				}

				if(data.from){

					let fromId = obj.fn.unstubbify(data.from);
					if(fromId){
						rData.from = fromId;
					}

				}

				if(data.upTo){

					let upTo = obj.fn.unstubbify(data.upTo);
					if(upTo){
						rData.upTo = upTo;
					}

				}

				if(data.parent){

					let parentId = obj.fn.unstubbify(data.parent);
					if(parentId){
						rData.parent = parentId;
					}

				}

				if(data.user){

					rData.user = data.user;

				}

				if(data.orderBy){

					rData.orderBy = data.orderBy;
					
				}

				if(data.category){

					switch(data.category) {

						case 'likes':
							data.orderBy = 'votesUp';
							break;
						case 'dislikes':
							data.orderBy = 'votesDown';
							break;
						case 'new':
							data.orderBy = 'idUp';
							break;
						case 'old':
							data.orderBy = 'idDown';
							break;

					}

				}

				return rData;

			},

		},

		get:(data,options)=>{

			return new Promise((resolve,reject)=>{

				if(!options){
					options={};
				}

				// Maximum time to show from
				if(!options.maxTime){
					options.maxTime = 172800 // s
				}

				// Maximum rows
				if(!options.limit){
					options.limit = 25;
				}

				if(data.limit){
					options.limit = data.limit;
				}

				let rtn = {
					comments:[],
					rows:0
				};

				let verifiedData = obj.fn.verify(data,options);
				let pList = [

					new Promise((res,rej)=>{

						let param = [],
							sql = [];

						if(options.userId) {

							param = [
								options.userId,
								options.userId
							];
							sql.push(obj.sql.get.user);

						} else {

							sql.push(obj.sql.get.anon);

						}

						if(data.top){

							sql.push(obj.sql.param.top);
					
						}

						for (var prop in obj.sql.param) {

							let preamble = 'AND';
							if(verifiedData[prop]){

								if(sql.length === 1) {

									preamble = 'WHERE';

								}

								sql.push(preamble + ' ' + obj.sql.param[prop]);
								param.push(verifiedData[prop]);

							}

						}

						sql.push(obj.fn.order(verifiedData));
						sql.push('LIMIT ?');
						param.push(options.limit);

						// console.log(sql);
						// console.log(param);

						global._s.db.q(sql.join(' '),param)
							.then(comments=>{
								
								if(comments.length>0){

									rtn.comments = comments;

									let repliesToPromises = [];
									for(let i=0,c=comments.length;i<c;i++){

										repliesToPromises.push(
											new Promise((resS,rejS)=>{

												global._s.db.q(obj.sql.get.repliesTo,[comments[i].id])
													.then(repliesTo=>{

														if(repliesTo.length===1){

															comments[i].repliesTo = repliesTo[0].repliesTo;
													
														}
														resS();

													})
													.catch(e=>{

														rejS(e);
												
													})

												})

											);

									}
									Promise.all(repliesToPromises)
										.then(()=>{

											rtn.comments = obj.fn.clean(comments,options);
											res();
										
										})
										.catch(e=>{
										
											rej(e);
										
										});

								} else {

									res();

								}

							})
							.catch(e=>{

								rej(e);
							
							});

					}),

					new Promise((res,rej)=>{

						let sql = [
								obj.sql.get.rows
							],
							param = [];

						for (var prop in verifiedData) {

							let preamble = 'AND';
							if(obj.sql.param[prop]){

								if(sql.length === 1) {

									preamble = 'WHERE';

								}

								sql.push(preamble + ' ' + obj.sql.param[prop]);
								param.push(verifiedData[prop]);

							}

						}

						global._s.db.q(sql.join(' '),param)
							.then(rowRtn=>{

								if(rowRtn.length===1){

									rtn.rows = rowRtn[0].numrows;
										
								}
								res();

							})
							.catch(e=>{

								rej(e);
							
							});

					}),

				];

				Promise.all(pList)
					.then(()=>{

						resolve(rtn);
					
					})
					.catch(e=>{

						reject(e);
					
					});

			});

		},

		getById:(data,options)=>{

			return new Promise((resolve,reject)=>{

				if(!data.id){

					reject('No id');
				
				} else {

					let sql = [obj.sql.get.anon], 
						param = [];

					if(options) {

						if(options.userId) {
							
							sql = [obj.sql.get.user];

							param.push(options.userId);
							param.push(options.userId);
						
						}

					}

					sql.push('WHERE '+obj.sql.param.comment);
					param.push(data.id);

					global._s.db.q(sql.join(' '),param)
						.then(result=>{

							let final = obj.fn.clean(result,options);
							resolve(final[0]);

						})
						.catch(e=>{

							reject(e);
						
						});

				}

			});

		},

	}

	return obj;

};