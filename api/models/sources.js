'use strict';

module.exports = ()=>{

	let obj = {

		sql:{

			get:{
				
				anon:'SELECT ns.name, ns.stub, ns.logo, ns.type, ns.url_rss, ns.url_twitter, ns.country AS location_country, ns.language AS location_language, _dt_update AS updated, COALESCE(count_ni.num,0) AS stat_items, COALESCE(count_nss.num,0) AS stat_followers, COALESCE(count_nc.num,0) AS stat_comments, COALESCE(count_viUp.num,0) - COALESCE(count_viDown.num,0) AS stat_votes FROM `news.sources` AS ns LEFT JOIN (SELECT COUNT(*) AS num, _source_id FROM `news.items` GROUP BY _source_id) AS count_ni ON count_ni._source_id = ns.id LEFT JOIN (SELECT COUNT(*) AS num, source_id FROM `user.saved.sources` WHERE active = 1 GROUP BY source_id) AS count_nss ON count_nss.source_id = ns.id LEFT JOIN (SELECT COUNT(*) AS num, sNi._source_id FROM `news.comments` AS sNc INNER JOIN `news.items` sNi ON sNi.id = sNc.item_id GROUP BY sNi._source_id) AS count_nc ON count_nc._source_id = ns.id LEFT JOIN (SELECT COUNT(*) AS num, sNii._source_id FROM `user.voted.items` AS sNsvi INNER JOIN `news.items` sNii ON sNii.id = sNsvi.item_id WHERE sNsvi.dir = 1 GROUP BY sNii._source_id) AS count_viUp ON count_viUp._source_id = ns.id LEFT JOIN (SELECT COUNT(*) AS num, sNiii._source_id FROM `user.voted.items` AS sNsvii INNER JOIN `news.items` sNiii ON sNiii.id = sNsvii.item_id WHERE sNsvii.dir = 0 GROUP BY sNiii._source_id) AS count_viDown ON count_viDown._source_id = ns.id',
				user:'SELECT ns.name, ns.stub, ns.logo, ns.type, ns.url_rss, ns.url_twitter, ns.country AS location_country, ns.language AS location_language, _dt_update AS updated, COALESCE(nss.active,0) AS isFollowed, COALESCE(count_ni.num,0) AS stat_items, COALESCE(count_nss.num,0) AS stat_followers, COALESCE(count_nc.num,0) AS stat_comments, COALESCE(count_viUp.num,0) - COALESCE(count_viDown.num,0) AS stat_votes FROM `news.sources` AS ns LEFT JOIN (SELECT COUNT(*) AS num, _source_id FROM `news.items` GROUP BY _source_id) AS count_ni ON count_ni._source_id = ns.id LEFT JOIN (SELECT COUNT(*) AS num, source_id FROM `user.saved.sources` WHERE active = 1 GROUP BY source_id) AS count_nss ON count_nss.source_id = ns.id LEFT JOIN (SELECT COUNT(*) AS num, sNi._source_id FROM `news.comments` AS sNc INNER JOIN `news.items` sNi ON sNi.id = sNc.item_id GROUP BY sNi._source_id) AS count_nc ON count_nc._source_id = ns.id LEFT JOIN (SELECT COUNT(*) AS num, sNii._source_id FROM `user.voted.items` AS sNsvi INNER JOIN `news.items` sNii ON sNii.id = sNsvi.item_id WHERE sNsvi.dir = 1 GROUP BY sNii._source_id) AS count_viUp ON count_viUp._source_id = ns.id LEFT JOIN (SELECT COUNT(*) AS num, sNiii._source_id FROM `user.voted.items` AS sNsvii INNER JOIN `news.items` sNiii ON sNiii.id = sNsvii.item_id WHERE sNsvii.dir = 0 GROUP BY sNiii._source_id) AS count_viDown ON count_viDown._source_id = ns.id LEFT JOIN `user.saved.sources` nss ON nss.source_id = ns.id AND nss.user_id = ?',

				latestImages:'SELECT ni.img FROM `news.items` AS ni INNER JOIN `news.sources` ns ON ns.id = ni.source_id WHERE ns.stub = ? ORDER by id DESC LIMIT ?',
			
			},

			conditionals:{

				source:'ns.stub = ?',
				country:'ns.country = ?',
				language:'ns.language = ?',
				type:'ns.type = ?',

			},

			orderBy:{

				// categories
				idUp:'ORDER BY ns.id DESC',
				idDown:'ORDER BY ns.id ASC',
				votesUp:'ORDER BY stat_votes DESC',
				votesDown:'ORDER BY stat_votes ASC',
				commentsUp:'ORDER BY stat_comments DESC',
				commentsUp:'ORDER BY stat_comments ASC',

			},

			limit: ' LIMIT ?',

		},

		fn:{

			conditionalize:(conditionals,data)=>{

				let sqlArr = [];
				for (var prop in obj.sql.conditionals) {

					let preamble = 'AND';
					if(data[prop]){

						if(sqlArr.length === 0) {

							preamble = ' WHERE';

						}
						sqlArr.push(preamble);
						sqlArr.push(obj.sql.conditionals[prop]);
						conditionals.push(data[prop]);

					}

				}
				return {
					conditionals:conditionals,
					sql:sqlArr.join(' ')
				};

			},

			order:(data)=>{

				let sql = ' '+obj.sql.orderBy.idUp;
				if(data.orderBy){

					sql = ' '+obj.sql.orderBy[data.orderBy];

				}
				return sql;

			},

		},

		verify:(data)=>{

			let rData = {};

			if(data.from){

				let fromId = obj.fn.unstubbify(data.from);
				if(fromId){
					rData.from = fromId;
				}

			}

			if(data.country) {

				data.country = data.country.toLowerCase();
				if(data.country.length !== 2) {
					rData.country = 'us';
				} else {
					rData.country = data.country;
				}

			}

			if(data.language) {

				data.language = data.language.toLowerCase();
				if(data.language.length !== 2) {
					rData.language = 'en';
				} else {
					rData.language = data.language;
				}

			}

			if(data.category){

				switch(data.category) {

					case 'likes':
						data.orderBy = 'votesUp';
						break;
					case 'comments':
						data.orderBy = 'commentsUp';
						break;
					case 'dislikes':
						data.orderBy = 'votesDown';
						break;
					case 'new':
						data.orderBy = 'idUp';
						break;

				}

			}

			if(data.orderBy){

				if(data.orderBy in obj.sql.orderBy){

					rData.orderBy = data.orderBy;

				}

			}

			if(data.user){

				rData.user = data.user;

			}

			if(data.source){

				rData.source = data.source;

			}

			if(data.sourceType){

				rData.type = data.sourceType;
				
			}


			return rData;

		},

		get:(data,options)=>{

			return new Promise((resolve,reject)=>{

				if(!options){
					options={};
				}

				// Maximum rows
				if(!options.limit){
					options.limit = 50;
				}

				let conditionals = [],
					sql = obj.sql.get.anon;

				if(options.userId) {

					conditionals.push(options.userId);
					sql = obj.sql.get.user;
				
				}

				let conditionalObj = obj.fn.conditionalize(conditionals,data);
				if(conditionalObj){
					if((conditionalObj.conditionals) && (conditionalObj.sql)){

						sql += conditionalObj.sql;
						conditionals = conditionalObj.conditionals;
					
					}
				}

				let order = obj.fn.order(data);
				if(order){
					sql += order;
				}
				
				if(options.limit > 0) {
					sql += obj.sql.limit;
					conditionals.push(options.limit);					
				}

				global._s.db.q(sql,conditionals)
					.then(items=>{

						resolve(items);
					
					})
					.catch(e=>{
					
						reject(e);
					
					});

			});

		}

	}

	return obj;

};