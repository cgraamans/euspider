'use strict';

// Initialize Application In global variable.
global._s = {

	fs:require('fs'),
	_d:{
		services:[],
		log:[],
		models:[],
	},

	db:require('./lib/db'),
	auth:require('./lib/auth'),
	log:require('./lib/log'),
	

	model:require('./lib/model'),
	models:{},

	opt:{
		db:require('./options/db'),
		socket:require('./options/socket'),
		auth:require('./options/auth'),
		routes:require('./options/routes'),
	},
	crypto:{
		jwt:require('jsonwebtoken'),
		SHA256:require('crypto-js/sha256'),
		base62:require('base62'),
	},
	loglevel:'D',
	
};

try {

	global._s.db.createPool(global._s.opt.db.settings)
		.then(function(pool){

			global._s.log('✔ MySQL Pool Created','D');
			global._s.db.pool = pool;

		})
		.catch(function(err){

			throw err;

		});

} catch(e) {

	global._s.log('! Error Creatign MySQL Pool','E');
	global._s.log(e,'E');
	process.exit(1);

}

try {

	global._s.io = require('socket.io')(global._s.opt.socket.port);
	
	global._s.log('✔ App Ready','I');
	global._s.log('. Socket open on port '+global._s.opt.socket.port,'D');

	global._s.io.on('connection', function(socket) {

			global._s.log('✔ New Connection','I');

			let $state = {
					timers:[],
					intervals:[],

					socket:socket,

				};

			$state.socket.join('main');

			$state.host = socket.conn.remoteAddress;
			if (socket.handshake.address.length > 3) {

				$state.host = socket.handshake.address;

			}
			if (socket.request.client._peername.address.length > 3) {

				$state.host = socket.request.client._peername.address;

			}
			if(socket.handshake.headers['x-forwarded-for']) {

				if(socket.handshake.headers['x-forwarded-for'].length > 3) {

					$state.host = socket.handshake.headers['x-forwarded-for'];
				
				}

			}
			
			if($state.host.length>3){

				global._s.log('✔ Host detected','D');
			
			}

			global._s.auth.bans($state.host)
				.then(bansByHost=>{

					let usr = {
						ok:false
					};

					if(bansByHost){

						global._s.log('! Banned User '+$state.host+' tried accessing the API','I');
						usr.bans = bansByHost;

					} else {

						$state.secret = usr.secret = global._s.crypto.SHA256(socket.id + (new Date()).getTime() + $state.host).toString();
						usr.ok = true;

					}

					$state.socket.emit('init',usr);
					global._s.log('✔ Socket: init emitted','D');

					// Controller routes
					global._s.opt.routes.route.forEach(route=>{

						$state.socket.on(route.sock,_d=>{

							global._s.auth.decode(_d,$state.secret)
								.then(data=>{

									global._s.auth.token.verify(data)
										.then(u=>{

											if(u){

												if(u.payload){

													let check = false;
													if(data.user) {

														check = data.user;

													}
													if(data._user) {

														check = data._user;

													}
													if(check){

														let upd = false;
														for (let checkKey in check) {

															if(u.payload[checkKey]) {

																if(check[checkKey] !== u.payload[checkKey]) {

																	upd = true;
																
																}

															} else {

																upd = true;
															
															}

														}
														if(upd) {

															global._s.auth.emit('auth',{ok:true,user:u.payload,action:'update'},$state);

														}

													}
												
												}
												if(u.u) {

													$state.user = u.u;	
												
												}

											}
											
											require(route.controller)(route.sock,$state,data)
												.catch(e=>{

													global._s.log('x Controller '+route.sock+' Error','D');
													global._s.log(e,'E',true);

												});

										})
										.catch(e=>{

											global._s.log('x Error in Verification of token for user','D');
											global._s.log(e,'E',true);

											if(e.m){
											
												global._s.auth.emit('auth',{ok:false,e:e.m,action:'logout'},$state);
											
											}
											

										});

								})
								.catch(e=>{

									global._s.log('x Decode Error','D');
									global._s.log(e,'D',true);

								});

						});

					});

			});

			$state.socket.on('disconnect',function(){

				global._s.log('. Lost Connection','I');
				if($state.user){
	
					if($state.user.auth === 1) {

						for(var a=0,c=global._s._d.services.length;a<c;a++){

							if($state.user.name === global._s._d.services[a].name){

								global._s._d.services.splice(a,1);

							}

						}

					}

				}

				if($state.intervals.length>0){

					global._s.log('. Stopping intervals','D');
					for(var a=0,c=$state.intervals.length;a<c;a++){
						clearInterval($state.intervals[a]);
					}

				}

				if($state.timers.length>0) {

					global._s.log('. Stopping timers','D');
					for(var a=0,c=$state.timers.length;a<c;a++){
						clearTimeout($state.timers[a]);
					}

				}

				$state = {};
				global._s.log('. State destroyed','D');

			});

	});

} catch(e) {

	global._s.log('x Fatal error in Socket Connection run','E');
	global._s.log(e,'E',true);

	process.exit(1);

}