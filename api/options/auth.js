'use strict';

module.exports = {

	// time to live for token before user has to log in with a password (in seconds)
	maxTokenTTL:259200,

	// Token TTL refresh time (pushed to client -> in seconds);
	tokenTTL:3600,

	// Minimum Password Score
	minPwdScore:55,

};