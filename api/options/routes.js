module.exports = {

/*

	RESERVED ROUTES FOR DEFAULTS:
		'auth-register',
		'auth',
		'services',
		'log'

*/

	route: [

		// Main Data Routes
		{
			
			sock:'items',
			controller:'./controllers/items',

		},

		{
			
			sock:'items-ping',
			controller:'./controllers/items-ping',

		},


		{
			
			sock:'comments',
			controller:'./controllers/comments',

		},

		{
			
			sock:'source',
			controller:'./controllers/source',

		},

		{
			
			sock:'sources',
			controller:'./controllers/sources',

		},

		// User Actions
		{
			
			sock:'user-vote',
			controller:'./controllers/user/vote',

		},

		{
			
			sock:'user-comment',
			controller:'./controllers/user/comment',

		},

		{
			
			sock:'user-comment-action',
			controller:'./controllers/user/comment-action',

		},

		{
			
			sock:'user-save',
			controller:'./controllers/user/save',

		},

		{
			
			sock:'user-settings',
			controller:'./controllers/user/settings',

		},

		// defaults

		{
			
			sock:'auth-register',
			controller:'./controllers/defaults/auth.register',

		},

		{
			
			sock:'auth',
			controller:'./controllers/defaults/auth',

		},


		{
			
			sock:'log',
			controller:'./controllers/defaults/log',

		},


		{
			
			sock:'services',
			controller:'./controllers/defaults/services',

		},


		{
			
			sock:'disconnect',
			controller:'./controllers/disconnect',

		},

	]

}