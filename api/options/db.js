'use strict';

module.exports = {
		
	settings:{
		user:process.env.EUSPIDER_DB_USERNAME,
		password:process.env.EUSPIDER_DB_PASSWD,
		database:process.env.EUSPIDER_DB,
		host:'localhost',
		charset:'utf8mb4'
	},
	retries:100,

};