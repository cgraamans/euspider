'use strict';

exports = module.exports = function(route,$state,data) {

	return new Promise((resolve,reject)=>{

		// model
		global._s.model.load('items');

		// Maximum time to show from
		if(!data.time){

			if(!data.from && !data.up && !data.item && !data.savedBy){

				data.time = 604800; // s

			}
			
		}

		//Set variables
		let options = {},
			rtn = {
				ok:false,
				category:data.category,
			};

		if(data._crc) {
			rtn._crc = data._crc;
		}

		// options
		if($state.user){
			
			if($state.user.id){

				options.userId = $state.user.id;
				options.userName = $state.user.name;
			
			}

		}

		let verifiedData = global._s.models.items.verify(data,options);
		if(verifiedData.ok === true){

			global._s.models.items.get(verifiedData,options)
				.then(items=>{						

					rtn.items = [];
					if(items.length>0){

						let cleanItems = global._s.models.items.clean(items);
						if(cleanItems.length >0 ){
							
							rtn.ok = true;
							rtn.items = cleanItems;	

						} else {
						
							rtn.err ='Error in cleaning item';
						
						} 
						
					} else {
						
						rtn.ok = true;
					
					}
					global._s.auth.emit(route,rtn,$state);


				})
				.catch(e=>{

					rtn.err = e;
					global._s.auth.emit(route,rtn,$state);


				});	

		} else {

			global._s.auth.emit(route,verifiedData,$state);	

		}

		resolve();

	});

};