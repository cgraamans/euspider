'use strict';

exports = module.exports = function(route,$state,data) {

	return new Promise((resolve,reject)=>{
		
		if($state.user) {

			if ($state.user.id) {

				if(!data.action){
					data.action = 'get';
				}
				if(!data.type) {
					data.type = 'secret'
				}

				if(data.action === 'get') {

					if(data.type === 'email') {

						let rtn = {ok:false,type:'email'};
						global._s.auth.check.email($state.user.id)
							.then((email)=>{

								rtn.ok = true;
								rtn.email = email;
								global._s.auth.emit(route,rtn,$state);

							});

					}

				}

				if(data.action === 'set') {

					if (data.type === 'secret') {

						let usr = {
							ok:true,
						};
						$state.secret = usr.secret = global._s.crypto.SHA256($state.socket.id + (new Date()).getTime() + $state.host).toString();
						$state.socket.emit('init',usr);

					}
					
					if (data.type === 'token') {

						let rtn = {ok:false,type:'token'};
						global._s.auth.token.create($state.user.id)
							.then(token=>{

								rtn.token = token;
								rtn.ok = true;

								global._s.auth.emit(route,rtn,$state);

							})
							.catch(e=>{

								global._s.auth.emit(route,rtn,$state);

							});

					}

					if ((data.type === 'password') && (data.password)) {

						let pwdCheck = global._s.auth.check.password(data.password),
							bad = [],
							rtn = {ok:false,type:'password'};

						if(pwdCheck){

							if(pwdCheck.strong !== true){

								pwdCheck.errors.forEach(e=>{
									bad.push(e);
								});

							}

						} else {

							bad.push('Error checking password');

						}

						if(bad.length<1){

							global._s.auth.encrypt(data.password,true)
								.then(pwd=>{

									global._s.db.q('UPDATE `users` SET password = ? WHERE id = ?',[pwd,$state.user.id])
										.then(()=>{

											rtn.ok = true;
											global._s.auth.emit(route,rtn,$state);

										})
										.catch(e=>{
											
											bad.push(e);

											rtn.e = bad;
											global._s.auth.emit(route,rtn,$state);

										});

								});

						} else {
				
							rtn.e = bad;
							global._s.auth.emit(route,rtn,$state);

						}

					}

					if((data.type === 'email') && (data.email)) {

						let re = /^(([^<>()\[\]\.,;:\s@\"]+(\.[^<>()\[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i;
						
						let t = re.test(data.email),
							rtn = {ok:false,type:'email'};

						if(t === true){
							
							global._s.db.q('UPDATE users SET email = ? WHERE id = ?',[data.email,$state.user.id])
								.then(()=>{

									global._s.auth.check.email($state.user.id)
										.then((email)=>{

											rtn.ok = true;
											rtn.email = email;

											global._s.auth.emit(route,rtn,$state);

										});

								})
								.catch(e=>{

									rtn.e = e;
									global._s.auth.emit(route,rtn,$state);

								});

						} else {

							rtn.e = 'Bad email address';
							global._s.auth.emit(route,rtn,$state);

						}

					}

				}

			}
		
		}

		resolve();

	});

};