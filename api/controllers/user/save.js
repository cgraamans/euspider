'use strict';

exports = module.exports = function(route,$state,data) {

	let v = {

		prep:()=>{

			return new Promise((resolve,reject)=>{

				let rtn = {
					sql:'REPLACE INTO `user.saved.items` (user_id,item_id,active,dt) VALUES (?)',
				};

				if((data.source) || (data.item) || (data.comment)) {

					if(data.source) {

						global._s.db.q('SELECT id FROM `news.sources` WHERE stub = ?',[data.source])
							.then(sourceIds=>{

								if(sourceIds.length===1){

									rtn.id = sourceIds[0].id;
									rtn.sql = 'REPLACE INTO `user.saved.sources` (user_id,source_id,active,dt) VALUES (?)';
									resolve(rtn);

								} else {

									reject('Source not found');

								}

							})
							.catch(e=>{

								reject(e);

							});
					
					}

					if(data.item) {

						rtn.id = global._s.crypto.base62.decode(data.item);
						if(rtn.id){

							resolve(rtn);

						} else {

							reject('Invalid item stub');

						}

					}

					if(data.comment) {

						rtn.id = global._s.crypto.base62.decode(data.comment);
						if(rtn.id){

							rtn.sql = 'REPLACE INTO `user.saved.comments` (user_id,comment_id,active,dt) VALUES (?)';
							resolve(rtn);

						} else {

							reject('Invalid comment stub');

						}

					}

				} else {

					reject('No valid data types');

				}

			});

		}

	};

	return new Promise((resolve,reject)=>{
		
		if($state.user) {

			if ($state.user.id) {

				let rtn = {
					ok:false,
				};
				if(data._crc){
					rtn._crc = data._crc;
				}

				v.prep()
					.then(usables=>{

						if(!data.save){

							data.save = 0;
						
						}

						if(data.save < 0){

							data.save = 0;
						
						}

						if(data.save > 0){

							data.save = 1;
						
						}

						let qArr = [
							$state.user.id,
							usables.id,
							data.save,
							Math.round((new Date()).getTime()/1000)
						];

						global._s.db.q(usables.sql,[qArr])
							.then(()=>{

								rtn.ok = true;
								rtn.save = data.save;
								
								if(data.source){
									rtn.source=data.source;
								}
								if(data.item){
									rtn.item=data.item;
								}
								if(data.comment){
									rtn.comment=data.comment;
								}

								global._s.auth.emit(route,rtn,$state);

							})
							.catch(e=>{

								rtn.e = e;
								global._s.auth.emit(route,rtn,$state);

							});


					})
					.catch(e=>{

						rtn.e = e;
						global._s.auth.emit(route,rtn,$state);

					});

			}
		
		}

		resolve();

	});

};