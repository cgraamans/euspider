'use strict';

exports = module.exports = function(route,$state,data) {

	return new Promise((resolve,reject)=>{

		let validate = ()=>{

			let ok = true;
			if(!$state.user) {
			
				ok = 'Not logged in';
			
			} else {
			
				if (!$state.user.id) {
			
					ok = 'Invalid User';
			
				}	
			
			}

			 if((!data.remove) && (!data.delete) && (!data.report) && (!data.ban)){

			 	ok = 'Missing valid key';

			 }

			if(data.remove){
			
				data.comment = data.remove;

			}

			if(data.report){
			
				data.comment = data.report;

			}

			if(data.delete){
			
				data.comment = data.delete;

			}

			if(data.comment){
	
				data.comment = global._s.crypto.base62.decode(data.comment);
				if(!data.comment){

					ok = 'Error decoding comment stub';
				
				}

			}

			return ok;

		};

		let isOk = validate();
		if(isOk !== true){

			global._s.auth.emit(route,{ok:false,e:isOk},$state)

		} else {

			global._s.model.load('comments');
			
			if(data.delete){

				global._s.models.comments.getById({id:data.comment},{userId:$state.user.id})
					.then(row=>{

						if(row.isByUser === 1){

							global._s.db.q("UPDATE `news.comments` SET isDeleted = 1 WHERE id = ?",[data.comment])
								.then(()=>{

									global._s.models.comments.getById({id:data.comment},{userId:$state.user.id})
										.then(cleanRow=>{

											let rtnVal = {ok:true,row:cleanRow};
											if(data._crc){
												rtnVal._crc = data._crc;
											}
											global._s.auth.emit(route,rtnVal,$state);

										})
										.catch(e=>{

											global._s.auth.emit(route,{ok:false,e:e},$state);

										});
		
								})
								.catch(e=>{

									global._s.auth.emit(route,{ok:false,e:e},$state);

								});

						}

					})
					.catch(e=>{

						global._s.auth.emit(route,{ok:false,e:e},$state);

					});

			}

			if(data.report){

				global._s.db.q("REPLACE INTO `news.comment.reported` (user_id,comment_id,dt) VALUES (?)",[[
							$state.user.id,
							data.comment,
							Math.round((new Date()).getTime()/1000)
						]])
					.then(()=>{

						global._s.models.comments.getById({id:data.comment},{userId:$state.user.id})
							.then(cleanRow=>{

								let rtnVal = {ok:true,row:cleanRow};
								if(data._crc){
									rtnVal._crc = data._crc;
								}
								global._s.auth.emit(route,rtnVal,$state);

							})
							.catch(e=>{

								global._s.auth.emit(route,{ok:false,e:e},$state);

							});

					})
					.catch(e=>{

						global._s.auth.emit(route,{ok:false,e:e},$state);

					});

			}

			if(data.remove) {

				if($state.user.auth>1){

					global._s.db.q("UPDATE `news.comments` SET isRemoved = 1, isRemovedBy = ? WHERE id = ?",[$state.user.id,data.comment])
						.then(()=>{

							global._s.models.comments.getById({id:data.comment},{userId:$state.user.id})
								.then(cleanRow=>{

									let rtnVal = {ok:true,row:cleanRow};
									if(data._crc){
										rtnVal._crc = data._crc;
									}
									global._s.auth.emit(route,rtnVal,$state);

								})
								.catch(e=>{

									global._s.auth.emit(route,{ok:false,e:e},$state);

								});

						})
						.catch(e=>{

							global._s.auth.emit(route,{ok:false,e:e},$state);

						});

				}

			}

			if(data.ban) {

				if($state.user.auth>2){

					global._s.db.q('SELECT id FROM `users` WHERE name = ?',[data.ban])
						.then((bannee)=>{

							if(bannee.length>0) {

								let ins = {

									user_id:bannee[0].id,
									by_admin:$state.user.id,
									dt:Math.round((new Date()).getTime()/1000),
									active:1,
								
								};

								if(data.reason){
								
									ins.reason = data.reason;
								
								}

								global._s.db.q('INSERT INTO `user.bans` SET ?',ins)
									.then(()=>{

										global._s.models.comments.getById({id:data.comment},{userId:$state.user.id})
											.then(cleanRow=>{

												let rtnVal = {ok:true,row:cleanRow};
												if(data._crc){
													rtnVal._crc = data._crc;
												}
												global._s.auth.emit(route,rtnVal,$state);

											})
											.catch(e=>{

												global._s.auth.emit(route,{ok:false,e:e},$state);

											});

									})
									.catch(e=>{

										global._s.auth.emit(route,{ok:false,e:e},$state);

									});

							} else {

								global._s.auth.emit(route,{ok:false,e:'User not found.'},$state);

							}

						})
						.catch(e=>{

							global._s.auth.emit(route,{ok:false,e:e},$state);

						});

				}

			}

		}

		resolve();

	});

};