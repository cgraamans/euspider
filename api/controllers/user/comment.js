'use strict';

exports = module.exports = function(route,$state,data) {

	return new Promise((resolve,reject)=>{

		let validate = ()=>{

			let ok = true;
			if(!$state.user) {
			
				ok = 'Not logged in';
			
			} else {
			
				if (!$state.user.id) {
			
					ok = 'Invalid User';
			
				}	
			
			}

			if(!data.txt){
			
				ok = 'No text';
			
			 } else {

				if(data.txt.length<1){
				
					ok = 'No text';
				
				} else {

					data.txt.replace(/<\/?[^>]+(>|$)/g, "");
					if(data.txt.length<1) {
		
						ok = "No text";
		
					}

				}

			 }

			 if((!data.item) && (!data.comment) && (!data.edit)){

			 	ok = 'Missing valid key';

			 }

			if(data.edit){
			
				data.comment = data.edit;

			}

			if(data.item){
			
				data.item = global._s.crypto.base62.decode(data.item);
				if(!data.item){

					ok = 'Error decoding item stub';
				
				}

			}

			if(data.comment){
	
				data.comment = global._s.crypto.base62.decode(data.comment);
				if(!data.comment){

					ok = 'Error decoding comment stub';
				
				}

			}

			return ok;

		};

		let isOk = validate();
		if(isOk !== true){

			global._s.auth.emit(route,{ok:false,e:isOk},$state)

		} else {

			let ins = {

				txt:data.txt,
				user_id:$state.user.id,
				dt:Math.round((new Date()).getTime()/1000)

			};

			if(data.item) {

				ins.item_id = data.item;

			}

			if(data.comment){

				ins.parent_id=data.comment;

			}

			if(data.edit){

				global._s.model.load('comments');
				global._s.models.comments.getById({id:data.comment},{userId:$state.user.id})
					.then(row=>{

						if((row.isByUser === 1) && (row.isRemoved === 0) && (row.isDeleted === 0)){

							global._s.db.q("UPDATE `news.comments` SET txt = ?, dt_edit = ? WHERE id = ?",[
									data.txt,
									Math.round((new Date()).getTime()/1000),
									data.comment
								])
								.then(()=>{

									global._s.models.comments.getById({id:data.comment},{userId:$state.user.id})
										.then(cleanRow=>{

											let rtnVal = {ok:true,row:cleanRow};
											if(data._crc){
												rtnVal._crc = data._crc;
											}

											global._s.auth.emit(route,rtnVal,$state);

										})
										.catch(e=>{

											global._s.auth.emit(route,{ok:false,e:e},$state);

										});

								})
								.catch(e=>{
									
									global._s.auth.emit(route,{ok:false,e:e},$state);

								});

						} else {

							global._s.auth.emit(route,{ok:false,e:'You are not authorized to edit this comment.'},$state);

						}

					})
					.catch(e=>{

						global._s.auth.emit(route,{ok:false,e:e},$state);

					});

			}

			if(!data.edit) {

				global._s.db.q("INSERT INTO `news.comments` SET ?",ins)
					.then(insRes=>{

						global._s.model.load('comments');
						global._s.models.comments.getById({id:insRes.insertId},{userId:$state.user.id})
							.then(row=>{

								let rtnVal = {ok:true,row:row};
								if(data._crc){
									rtnVal._crc = data._crc;
								}
								global._s.auth.emit(route,rtnVal,$state);

							})
							.catch(e=>{

								global._s.auth.emit(route,{ok:false,e:e},$state);

							});

					})
					.catch(e=>{
						
						global._s.auth.emit(route,{ok:false,e:e},$state);

					});

			}

		}

		resolve();

	});

};