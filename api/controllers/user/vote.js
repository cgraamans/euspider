'use strict';

exports = module.exports = function(route,$state,data) {

	return new Promise((resolve,reject)=>{
		
		if($state.user) {

			if ($state.user.id) {

				let passId = false,
					sqlReplace = 'REPLACE INTO `user.voted.items` (user_id,item_id,dir,dt) VALUES (?)',
					sqlLookup = 'SELECT (COALESCE(l.votes,0) - COALESCE(dl.votes,0)) AS votes FROM `news.items` as ni LEFT JOIN (SELECT COUNT(*) AS votes, item_id FROM `user.voted.items` WHERE dir = 1 GROUP BY item_id) AS l ON l.item_id = ni.id LEFT JOIN (SELECT COUNT(*) AS votes, item_id FROM `user.voted.items` WHERE dir < 0 GROUP BY item_id) AS dl ON dl.item_id = ni.id WHERE ni.id = ?',
					rtn = {
						ok:false,
					};

				if(data.item){

					rtn.item = data.item;
					passId = global._s.crypto.base62.decode(data.item);

				}

				if(data.comment) {

					rtn.comment = data.comment;
					passId = global._s.crypto.base62.decode(data.comment);
					sqlReplace = 'REPLACE INTO `user.voted.comments` (user_id,comment_id,dir,dt) VALUES (?)';
					sqlLookup = 'SELECT (COALESCE(voteUp.vote,0) - COALESCE(voteDown.vote,0)) AS votes FROM `news.comments` AS nc LEFT JOIN (SELECT count(*) AS vote, comment_id FROM `user.voted.comments` WHERE dir>0 GROUP BY comment_id) voteUp ON voteUp.comment_id = nc.id LEFT JOIN (SELECT count(*) AS vote, comment_id FROM `user.voted.comments` WHERE dir<0 GROUP BY comment_id) voteDown ON voteDown.comment_id = nc.id WHERE nc.id = ?';

				}

				if(passId !== false) {

					if(!data.vote){

						data.vote = 0;
					
					}

					if(typeof data.vote !== 'number'){
						
						data.vote = 0;
					
					}

					if(data.vote < 0){

						data.vote = -1;
					
					}

					if(data.vote > 0){

						data.vote = 1;
					
					}

					let mData = [
						$state.user.id,
						passId,
						data.vote,
						Math.round((new Date()).getTime()/1000)
					];

					global._s.db.q(sqlReplace,[mData])
						.then(()=>{

							global._s.db.q(sqlLookup,[passId])
								.then(votesArrOfObj=>{

									if(votesArrOfObj.length > 0){

										rtn.ok = true;
										rtn.votes = votesArrOfObj[0].votes;
										rtn.vote = data.vote;

										if(data._crc){
											rtn._crc = data._crc;
										}

									} else {

										rtn.e = 'No votes returned';

									}

									global._s.auth.emit(route,rtn,$state);

								})
								.catch(e=>{

									rtn.e = e;
									global._s.auth.emit(route,rtn,$state);

								});							

						})
						.catch(e=>{

							rtn.e = e;
							global._s.auth.emit(route,rtn,$state);

						});

				} else {

					rtn.e = 'Invalid ID';
					global._s.auth.emit(route,rtn,$state);

				}

			}
		
		}

		resolve();

	});

};