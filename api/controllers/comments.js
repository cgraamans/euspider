'use strict';

exports = module.exports = function(route,$state,data) {

	return new Promise((resolve,reject)=>{

		//Set variables
		let options = {},
			rtn = {
				ok:false,
				comments:[],
				rows:0,
			};

		if(data._crc) {
			rtn._crc = data._crc;
		}

		// model
		global._s.model.load('comments');

		// options
		if($state.user){
			
			if($state.user.id){

				options.userId = $state.user.id;
				options.userName = $state.user.name;
			
			}

		}

		// Maximum time to show from
		if(!data.time){
			data.time = 604800; // s
		}

		global._s.models.comments.get(data,options)
			.then(cObj=>{

				rtn.comments = cObj.comments;
				rtn.rows = cObj.rows;
				rtn.ok = true;

				global._s.auth.emit(route,rtn,$state);

			})
			.catch(e=>{

				rtn.err = e;
				
				global._s.auth.emit(route,rtn,$state);

			});


		resolve();

	});

};