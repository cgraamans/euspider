'use strict';

'use strict';

exports = module.exports = function(route,$state,data) {

	return new Promise((resolve,reject)=>{

		global._s.auth.login(data)
			.then(verified=>{

				$state.user = verified.u;
				if($state.user.name) {

					$state.socket.nickname = $state.user.name;

				}

				if(verified.payload){

					global._s.auth.emit('auth',{ok:true,user:verified.payload},$state);

				}

				global._s.log('✔ Socket: user '+$state.user.id+ ' logged in','I');

				if($state.user.auth === 1) {

					global._s.auth.emit('auth',{ok:true},$state);
					global._s._d.services.push({

						name:$state.user.name,

					});

				}
				resolve();

			})
			.catch(e=>{
				
				if(e.e){

					global._s.log('x Error in verify','E');
					reject(e.e);

				} else {
					
					e.ok = false;

					if(e.m) {

						global._s.log('! Bad User','D');

					}
					if(e.bans) {

						global._s.log('! Banned User','D');

					}
					global._s.log(e,'D',true);
					global._s.auth.emit('auth',e,$state);

					resolve();

				}

			});

	});

};
