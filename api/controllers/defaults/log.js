'use strict';

exports = module.exports = function(route,$state,data) {

	return new Promise((resolve,reject)=>{

		if (data) {

			if($state.user) {

				if($state.user.auth > 0) {

					global._s._d.log.unshift(data);
					resolve();

				} else {

					reject('Wrong user authorization');

				}


			} else {

				reject('User unauthorized');

			}


		} else {

			reject('No data.');

		}

	});

};