'use strict';

exports = module.exports = function(route,$state,data) {

	return new Promise((resolve,reject)=>{
		
		global._s.log('✔ Starting services interval','D');

		global._s.auth.emit(route,{ok:true,services:global._s._d.services},$state);
		
		$state.intervals.push(setInterval(()=>{

			global._s.auth.emit(route,{
				ok:true,
				services:global._s._d.services,
				log:global._s._d.log,
			},$state);
			global._s.log('✔ Services emitted','D');

		},30000));

		resolve();

	});

};