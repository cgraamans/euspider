'use strict';

exports = module.exports = function(route,$state,data) {

	return new Promise((resolve,reject)=>{

		let bad = [];
		if(!data.name){
			bad.push('No name.');
		}

		if(!data.password){
			bad.push('No password.');
		}

		if(!data.name.match(/^[a-zA-Z0-9_\-\.]+$/)){
			bad.push('Invalid name.');
		}

		let pTest = global._s.auth.check.password(data.password);
		if(!pTest){

			bad.push('OWASP fail.');

		} else {

			if(pTest.strong !== true){

				pTest.errors.forEach(e=>{
					bad.push(e);
				});

			}

		}

		if(bad.length>0) {

			global._s.auth.emit(route,{ok:false,e:'Registration Failed',m:bad},$state);
			resolve();

		} else {

			if(!data.persistent){data.persistent=0}else{data.persistent=1}
			global._s.auth.check.name(data.name)
				.then(()=>{

					global._s.auth.encrypt(data.password,true)
						.then(pwd=>{

							let apiId = global._s.auth.token.random();
							let insUser = {
								name:data.name,
								password:pwd,
								api_id:apiId,
								dt_register:Math.round((new Date()).getTime()/1000)
							};

							global._s.db.q('INSERT INTO `users` SET ?',insUser)
								.then(u=>{

									if(!u.insertId){

										global._s.auth.emit(route,{ok:false,e:'Registration Failed'},$state);
										resolve();

									} else {

										global._s.auth.token.create(u.insertId,data.persistent)
											.then(token=>{

												$state.user = {
													id:u.insertId,
													auth:0,
													name:data.name
												};

												global._s.auth.emit(route,{ok:true,user:{
													apiId:apiId,
													token:token,
													name:data.name,
												}},$state);

												global._s.log('✔ Auth-Register: user '+data.name+ ' registered','I');
												
												resolve();

											})
											.catch(e=>{

												global._s.auth.emit(route,{ok:false,e:'Registration Failed'},$state);

												global._s.log('x '+route+': Create Token Error','D');
												global._s.log(e,'E');
									
												resolve();


											});

									}
								})
								.catch(e=>{
									
									global._s.auth.emit(route,{ok:false,e:'Registration Failed'},$state);

									global._s.log('x '+route+': Insert User Error','D');
									global._s.log(e,'E');

									resolve();

								});

						})
						.catch(e=>{

							global._s.auth.emit(route,{ok:false,e:'Registration Failed'},$state);

							global._s.log('x '+route+': Encryption Error','D');
							global._s.log(e,'E');

							resolve();
						
						});

				})
				.catch(e=>{

					let bc = {
						ok:false,
						e:'Registration Failed',
						m:['Name already registered.']
					};
					
					if(e){
						bc.m.push(e);
					}

					global._s.auth.emit(route,bc,$state);
				
				});

		}

	});

};