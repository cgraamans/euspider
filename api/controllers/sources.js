'use strict';

exports = module.exports = function(route,$state,data) {

	return new Promise((resolve,reject)=>{
		
		global._s.model.load('sources');

		// mOpts

		let mOpts = {
			limit: -1
		};
		if($state.user){
			
			if($state.user.id){

				mOpts.userId = $state.user.id;
			
			}

		}

		let rtn = {ok:false};
		if(data._crc){
			rtn._crc = data._crc;
		}

		global._s.models.sources.get(global._s.models.sources.verify(data),mOpts)
			.then(sources=>{

				rtn.ok = true;
				rtn.sources = sources;

				global._s.auth.emit(route,rtn,$state);


			})
			.catch(e=>{

				rtn.e = e;
				global._s.auth.emit(route,rtn,$state);

			});

		resolve();

	});

};