'use strict';

exports = module.exports = function(route,$state,data) {

	return new Promise((resolve,reject)=>{

		let rtn = {
			ok:false,
		};

		// model
		global._s.model.load('items');

		let cleanData = global._s.models.items.verify(data);
		global._s.models.items.count(cleanData)
			.then(rows=>{

				rtn.ok = true;
				rtn.count = rows;

				global._s.auth.emit(route,rtn,$state);

			})
			.catch(e=>{

				rtn.e = e;
				global._s.auth.emit(route,rtn,$state);

			});

			resolve();
	
	});

};