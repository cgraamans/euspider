'use strict';

exports = module.exports = function(route,$state,data) {

	return new Promise((resolve,reject)=>{

		//Set variables
		let mOpts = {};

		// model
		global._s.model.load('news');
		global._s.model.load('sources');

		// mOpts
		if($state.user){
			
			if($state.user.id){

				mOpts.userId = $state.user.id;
			
			}

		}

		if(data.source){

			global._s.models.sources.get(data,mOpts)
				.then(sources=>{

					global._s.auth.emit(route,{ok:true,source:sources[0]},$state);

				})
				.catch(e=>{

					global._s.auth.emit(route,{ok:false,err:e},$state);

				});

		}

		resolve();

	});

};