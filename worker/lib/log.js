'use strict';

module.exports = function(msg,level,noDate){

	let showCases = [],
		debugLvl = process.env.EUSPIDER_DEBUG;
	
	if(!debugLvl){
		if(global._s){
			if(global._s.loglevel) {
				debugLvl = global._s.loglevel;
			}
		}
	}
	switch(debugLvl) {

		case 'D':
		case true:
			showCases=['D','E','I'];
			break;

		case 'I':
			showCases=['I','E'];
			break;

		case 'E':
			showCases=['E'];
			break;

		default:
		    showCases=['E'];
		    break;
			
	}

	if(level) {

		let dateIns = '';
		if(!noDate){
			dateIns = new Date();
		}

		showCases.forEach(sC=>{

			if (sC === level) {
				console.log(dateIns + ' ' + msg);
			}

		});
	}

};