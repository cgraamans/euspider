'use strict';

module.exports = {

	encode:function(obj,secret){

		return new Promise((resolve)=>{

			let signed = global._s.crypto.jwt.sign(obj,global._s.opt.socket.secret);
			resolve(signed);

		});
		

	},
	decode:function(msg,secret){

		return new Promise((resolve)=>{

			var decoded = global._s.crypto.jwt.verify(msg,global._s.opt.socket.secret);
			resolve(decoded);

		});

	},

	// SHA256 encrypt
	encrypt:function(obj,isStr) {

		return new Promise((resolve,reject)=>{

			try {

				if(!isStr){
					obj = JSON.stringify(obj);
				}
				let d = global._s.crypto.SHA256(obj).toString();
				resolve(d);

			} catch(e) {

				reject(e);

			}

		});
			
	},



	
};