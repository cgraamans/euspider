'use strict';

module.exports = {

	options:require('../options/http'),
	_:{

		http:require('http'),

		write:function(options,data){

			var that = this;
			return new Promise((resolve,reject)=>{

				try {

					that.http.request(options, (response)=>{

						let str = '';
						response.on('data',function(chunk){
							str+=chunk;
						});

						response.on('end', function() {
							resolve(JSON.parse(str));
						});			

						response.on('error', function(err) {
							reject(err);
						});

					}).write(JSON.stringify(data));

				} catch(e) {

					reject(e);

				}

			});

		},

		read:function(options){

			var that = this;
			return new Promise((resolve,reject)=>{

				try {

					that.http.request(options, (response)=>{

						let str = '';
						response.on('data',function(chunk){
							str+=chunk;
						});

						response.on('end', function() {
							resolve(JSON.parse(str));
						});			

						response.on('error', function(err) {
							reject(err);
						});

					}).end();

				} catch(e){

					reject(e);

				}

			});
			
		}

	},

	get:function(path){

		var that = this;
		return new Promise((resolve,reject)=>{

			try {

				let options = JSON.parse(JSON.stringify(that.options));
				options.path = path;

				that._.read(options)
					.then(r=>{

						resolve(r);

					})
					.catch(e=>{

						reject(e);

					});

			} catch(e) {

				reject(e);

			}

		});

	},

	put:function(path,data){

		var that = this;
		return new Promise((resolve,reject)=>{

			try {

				let options = JSON.parse(JSON.stringify(that.options));

				options.path = path;
				options.method = 'PUT';
				
				that._.write(options,data)
					.then(r=>{

						resolve(r);

					})
					.catch(e=>{

						reject(e);

					});

			} catch(e) {

				reject(e);

			}

		});

	},

	post:function(path,data){

		var that = this;
		return new Promise((resolve,reject)=>{

			try {

				let options = JSON.parse(JSON.stringify(that.options));

				options.path = path;
				options.method = 'POST';
				
				that._.write(options,data)
					.then(r=>{

						resolve(r);

					})
					.catch(e=>{

						reject(e);

					});

			} catch(e) {

				reject(e);

			}

		});

	},

	delete:function(path){

		var that = this;
		return new Promise((resolve,reject)=>{

			try {

				let options = JSON.parse(JSON.stringify(that.options));

				options.path = path;
				options.method = 'DELETE';
				
				that._.read(options)
					.then(r=>{

						resolve(r);

					})
					.catch(e=>{

						reject(e);

					});

			} catch(e) {

				reject(e);

			}

		});

	}

};