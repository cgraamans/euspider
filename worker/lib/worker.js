'use strict';

module.exports = {

	emit:function(socket,data){

		return new Promise((resolve,reject)=>{

			global._s.auth.encode(data)
				.then(message=>{

					global._s.socket.emit(socket,message);
					global._s.log('✔ Message sent to socket '+socket,'D');

					resolve();

				})
				.catch(e=>{

					reject(e);
				
				});

		});

	},

	instance:function(payload){

		var that = this;
		return new Promise((resolve,reject)=>{

			let inst = require(global._s.opt.instances.location+payload.name);
			inst.init(payload)
				.then(interval=>{

					payload.obj = interval;
					resolve(payload);

				})
				.catch(e=>{

					reject(e);

				})


		});

	},

	dt:{

	    unixtime:function() {

	      return Math.round(new Date().getTime()/1000);

	    },

	    yyyyMMDD:function(){

			let MM = parseInt(new Date().getMonth()) + 1,
				DD = new Date().getDate();

			if(MM<10){
				MM = '0'+MM;
			}

			if(DD<10){
				DD = '0'+DD;
			}
 		 	return new Date().getFullYear() + "-" + MM + "-" + DD;

		},

    },

    f:{
		
		nl2br:(str, is_xhtml)=>{
  			let breakTag = (is_xhtml || typeof is_xhtml === 'undefined') ? '<br ' + '/>' : '<br>';
  			return (str + '').replace(/([^>\r\n]?)(\r\n|\n\r|\r|\n)/g, '$1' + breakTag + '$2');
		},
		br2nl:(str)=>{
			return str.replace(/<br\s*\/?>/mg,"\n");
		},
		

    },

    html:{

    	he:require('he'),
		decode:function(html,escapeQ){

			var that = this;

			let first = html.replace(/<\/?[^>]+(>|$)/g, "");
			let second = global._s.worker.f.br2nl(first);
			let third = that.he.decode(second);
			if(escapeQ){
				third = third.replace(/'/g, "\\'");
			}

			return third;

		},

    }

};