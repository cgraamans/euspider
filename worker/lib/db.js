'use strict';

module.exports = {

	_:{
		connectionRetryInterval:500, // millisecons
		connectionRetries:10,
	},

	// Create Connection
	createConnection:function(options){

		var that = this;
		return new Promise((resolve,reject)=> {

			try {

				var mysql = require('mysql');
				that.connection = mysql.createConnection(options);

				that.connection.connect(err=>{
					
					if(err){

						reject(err);
					
					} else {
					
						resolve();
					
					}

				});

			} catch(e) {

				reject(e);
			
			}

		});

	},

	// Execute MySQL query
	q:function(s,v){

		var that = this;
		return new Promise((resolve,reject)=> {

			try {

				if(that.connection) {
						
					that.connection.query(s,v,(error,res)=>{

						if(error) {
						
							reject(error);
						
						} else {

							resolve(res);

						}

					});	

				} else {

					reject('No connection.');

				}


			} catch(e) {

				reject(e);

			}

		});

	}

};