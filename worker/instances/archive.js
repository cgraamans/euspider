'use strict';

module.exports = {

	// SQL
	_s: {

		populate:'SELECT * FROM `news.items` WHERE _dt < ? AND id NOT IN (SELECT DISTINCT item_id FROM `news.comments`) AND id NOT IN (SELECT DISTINCT item_id FROM `user.saved.items`) AND id NOT IN (SELECT DISTINCT nc.item_id FROM `news.comments` AS nc INNER JOIN `user.saved.comments` usc ON usc.comment_id = nc.id);',
		crcCheck:'SELECT id FROM `news.item.archive` WHERE _crc = ?',
		copy:'INSERT INTO `news.item.archive` SET ?',
		remove:'DELETE FROM `news.items` WHERE _crc = ?',

	},

	// Config
	_d:{

		cleanup:604800, // archive time (in s)

	},

	rollback:function(){

		let that = this;
		return new Promise((resolve,reject)=>{

			global._s.db.q('ROLLBACK;',[])
				.then(()=>{	

					global._s.log('! ARCHIVE: rollback successful','I');
					resolve();
				
				})
				.catch(e=>{

					global._s.log('x ARCHIVE: rollback error!','E');					
					global._s.log(e,'E',true);
					reject(e);

				});

		});

	},

	commit:function() {

		let that = this;
		return new Promise((resolve,reject)=>{
					
			global._s.log('. ARCHIVE: committing','D');
			global._s.db.q('COMMIT;',[])
				.then(()=>{

					global._s.log('✔ ARCHIVE: commit successful','D');
					resolve();

				})
				.catch(e=>{

					global._s.log('x ARCHIVE: error committing transaction','E');
					global._s.log(e,'E',true);
					
					that.rollback()
						.then(()=>{

							resolve();
						
						})
						.catch(e=>{
							
							global._s.log('x ARCHIVE: rollback error!','E');					
							global._s.log(e,'E',true);

							reject();

						});

				});

		});

	},

	// initialization promise 
	init:function(options){

		let that = this;
		return new Promise((resolve,reject)=>{

			let localInterval = setInterval(()=>{

				let timeFrom = global._s.worker.dt.unixtime() - that._d.cleanup;
				global._s.db.q('START TRANSACTION;',[])
					.then(()=>{

						global._s.db.q(that._s.populate,[timeFrom])
							.then(populated=>{
			
								if(populated.length>0){

									let crcRun = [];
									populated.forEach(populatedItem=>{

										crcRun.push(new Promise((res,rej)=>{

											global._s.db.q(that._s.crcCheck,[populatedItem._crc])
												.then(crcChecked=>{

													if(crcChecked.length < 1){

														global._s.db.q(that._s.copy,populatedItem)
															.then(()=>{

																global._s.db.q(that._s.remove,[populatedItem._crc])
																	.then(()=>{
																		
																		res(populatedItem._crc);

																	})
																	.catch(e=>{

																		global._s.log('x ARCHIVE: error removing item '+populatedItem._crc,'E');
																		global._s.log(e,'E',true);

																		rej();

																	});


															}).catch(e=>{

																global._s.log('x ARCHIVE: error copying item '+populatedItem._crc,'E');
																global._s.log(e,'E',true);

																rej();

															});


													} else {

														global._s.log('! ARCHIVE: duplicate item for '+populatedItem._crc,'I');
														global._s.db.q(that._s.remove,[populatedItem._crc])
															.then(()=>{

																global._s.log('! ARCHIVE: duplicate item '+populatedItem._crc+' removed','D');

																res();
															
															})
															.catch(e=>{

																global._s.log('x ARCHIVE: error removing duplicate item '+populatedItem._crc,'E');
																global._s.log(e,'E',true);

																rej();

															});														

													}
													
												})
												.catch(e=>{

													global._s.log('x ARCHIVE: error checking CRC for '+populatedItem._crc,'E');
													global._s.log(e,'E',true);

													rej();

												});

										}));

									});

									if(crcRun.length>0) {

										Promise.all(crcRun)
											.then(crcCheckedPromiseArray=>{

												let crcCheckedCounter = 0;
												if(crcCheckedPromiseArray.length>0){

													crcCheckedCounter = 0;
													crcCheckedPromiseArray.forEach(crcCheckedItem=>{

														if(crcCheckedItem){

															crcCheckedCounter++;
														
														}
													
													});

												}

												global._s.log('✔ ARCHIVE: '+crcCheckedCounter+' items archived','I');

												that.commit();

											})
											.catch(()=>{

												global._s.log('x ARCHIVE: promise error','E');

												that.rollback();

											});

									} else {

										global._s.log('. ARCHIVE: no items archived','I');

										that.commit();

									}

								} else {
									
									global._s.log('. ARCHIVE: no items archived','I');

									that.commit();

								}

							})
							.catch(e=>{

								global._s.log('x ARCHIVE: error populating queue','E');
								global._s.log(e,'E',true);

								that.rollback();

							});


					})
					.catch(e=>{

						global._s.log('x ARCHIVE: error starting transaction','E');
						global._s.log(e,'E',true);

						that.rollback();

					});

					global._s.log('. ARCHIVE: running transaction','I');

			},options.interval); // interval

			resolve(localInterval);

		}); // promise

	}

};