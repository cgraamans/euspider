module.exports = {

	Twitter:require('twitter'),

	_tokens:{
		consumer_key: process.env.EUSPIDER_TW_C_KEY,
		consumer_secret: process.env.EUSPIDER_TW_C_SECRET,
		access_token_key: process.env.EUSPIDER_TW_AT_KEY,
		access_token_secret: process.env.EUSPIDER_TW_AT_SECRET,
	},

	_s:{

		insertNewsItem:'INSERT INTO `news.items` SET ?',
		sources:'SELECT id, stub, url_twitter, name FROM `news.sources` WHERE url_twitter IS NOT NULL AND isActive = 1',
		crcItem:'SELECT _crc FROM `news.items` WHERE _crc = ? UNION SELECT _crc FROM `news.item.archive` WHERE _crc = ?',		
	    updateSourceDT:'UPDATE `news.sources` SET _dt_update = ? WHERE id = ?',

	},

	// Data
	_d:{

		sources:[],
		intervals:[],

		// config for internal processing
		config:{
			
			allowRetweets:false,
			allowAllCharacters:false,

			intervals:60000, // feed's initial starting time variable
			sources:360000, // source starting time variable

		},

		forbiddenChars: [
			'@'
		],

		spool:{

			feeds:[],
			config:{
				limit:1, // number of inserts
				interval:300, // number of seconds between inserts
			},

		},

	},

	getUserTimelines:function(source) {

		var that = this;
		return new Promise((resolve, reject)=>{

			global._s.log('. TWI: fetching '+source.name,'D');
			that.client.get('statuses/user_timeline', {screen_name: source.url_twitter,tweet_mode:'extended'}, (e, tweets, response)=>{
				
				if (e) {

					reject(e);

				} else {

					let p = [],
						rtnObj = {
							
							items:[],
							errors:[],
							bad:[],
							duplicates:[],
							spooled:[],

							countForbidden:0,
							countRetweet:0,
							
						};

					if (tweets) {

						if(tweets.length > 0) {

							global._s.log('✔ TWI: fetched '+source.name,'I');

							tweets.forEach(tweet=>{
								
								if(tweet){

									p.push(new Promise((res,rej)=>{

										let isBad = false;

										if((!tweet.full_text) || (!tweet.id_str)) {

											isBad = 'missing twitter variables';

										}

										if(isBad === false) {

											if(that._d.config.allowAllCharacters){

												if(that._d.forbiddenChars){

													that._d.forbiddenChars.forEach(fo=>{

														if (tweet.full_text.includes(fo)) {

															rtnObj.countForbidden++;
															isBad = 'forbiddenChars';
														}

													});

												}

											}

											if(that._d.config.allowRetweets){

												if(tweet.retweeted_status) {

													if(tweet.retweeted_status !== null) {

														rtnObj.countRetweet++;
														isBad = 'retweet';

													}

												}

											}

										}

										if(isBad === false) {

											let item = {

												title:tweet.full_text,
												pubDate:tweet.created_at,
												_dt:global._s.worker.dt.unixtime(),
												_date:global._s.worker.dt.yyyyMMDD(),
												_source_id:source.id,
												
												img:tweet.user.profile_image_url_https,
												link:'https://twitter.com/i/web/status/'+tweet.id_str,
											
											};

											// DateTime override
											if(item.pubDate){

												let itemDate = new Date(item.pubDate);
												if(itemDate){

													let itemUnixDT = Math.round(itemDate.getTime()/1000);
													if(itemUnixDT){

														if((itemUnixDT.toString().length === 10) && (typeof itemUnixDT === 'number')){

															if(itemUnixDT < item._dt){

																item._dt = itemUnixDT;

															}

														}

													}

												}

											}

											// Entity Object Data
											if(tweet.entities){

												if(tweet.entities.urls){

													if(tweet.entities.urls.length>0){

														item.link = tweet.entities.urls[0].expanded_url;

													}

												}

												if(tweet.entities.media){

													if(tweet.entities.media.length>0){

														if (tweet.entities.media[0].media_url_https) {

															item.img = tweet.entities.media[0].media_url_https;

														}

													}

												}

											}

											// Filter URLs
											item.title = item.title.replace(/(?:https?|ftp):\/\/[\n\S]+/g, '');
											item.title = item.title.trim();

											if(!item.title){

												isBad = 'empty title';

											}

											if(isBad === false){

												// pack CRC and then put the data in the item object if there's a title and a link.
												global._s.auth.encrypt(item.title+'/'+source.id+'/'+item.link,true)
													.then(crc=>{

														item._crc = crc;

														global._s.db.q(that._s.crcItem,[item._crc,item._crc])
															.then(isDuplicate=>{

																if(isDuplicate.length === 0) {
																
																	let isInSpool = that._d.spool.feeds.findIndex(x=>x._crc = item._crc);
																	if(isInSpool < 0){

																		rtnObj.items.push(item);

																	} else {

																		rtnObj.spooled.push({name:source.name,item:item,tweet:tweet});

																	}

																} else {

																	rtnObj.duplicates.push({name:source.name,item:item,tweet:tweet});

																}

																res();

															})
															.catch(e=>{

																item.err = e;
																rtnObj.errors.push({name:source.name,item:item,tweet:tweet});
																res();


															});

													})
													.catch(e=>{

														item.err = e;
														rtnObj.errors.push({name:source.name,item:item,tweet:tweet});
														res();

													});


											} else {

												rtnObj.bad.push({name:source.name,tweet:tweet,error:isBad});
												res();

											}

										} else {

											rtnObj.bad.push({name:source.name,tweet:tweet,error:isBad});
											res();

										}

									}));

								}

							});

						} else {

							resolve(rtnObj);

						}

						if(p.length > 0) {

							Promise.all(p)
								.then(()=>{

									resolve(rtnObj);

								})
								.catch(e=>{

									reject(e);

								});

						} else {

							resolve(rtnObj);

						}

					} else {

						resolve(rtnObj);

					}

				}

			});

		});

	},

	setItems:function(source,options){

		var that = this;
		that.getUserTimelines(source)
			.then(userItemsObj=>{

				if(userItemsObj) {

					if(userItemsObj.duplicates.length > 0){

						global._s.log('. TWI: '+userItemsObj.duplicates.length+' duplicate items for '+source.name,'D');

					}

					if(userItemsObj.bad.length > 0){

						global._s.log('x TWI: '+userItemsObj.bad.length+' bad tweets for '+source.name,'D');

					}

					if(userItemsObj.spooled.length > 0){

						global._s.log('x TWI: '+userItemsObj.spooled.length+' already spooled for '+source.name,'D');

					}

					if(userItemsObj.errors.length > 0){
						
						global._s.log('x TWI: '+userItemsObj.errors.length+' database errors for '+source.name,'E');
						userItemsObj.errors.forEach(err=>{
							global._s.log(err.item.err,'E',true);
						})

					}				
					if(userItemsObj.items.length>0) {

						let countDupSpool = 0,
							countSpooling = 0;

						userItemsObj.items.forEach(itemToSpool=>{

							let isDuplicateSpool = that._d.spool.feeds.findIndex(x=>x._crc === itemToSpool._crc);
							if(isDuplicateSpool < 0) {

								that._d.spool.feeds.push(itemToSpool);
								countSpooling++;

							} else {

								countDupSpool++;

							}

						});

						if(countSpooling>0){

							global._s.log('✔ TWI: '+countSpooling+' items added to spool for '+source.name,'I');

						}

						if(countDupSpool>0){

							global._s.log('x TWI: '+countDupSpool+' duplicates in spool for '+source.name,'E');

						}

					}

				}

			})
			.catch(e=>{

				global._s.log('x TWI: error fetching '+source.name,'E');
				global._s.log(e,'E',true);

			});

	},

	interval:function(options){

		var that = this;
		return new Promise((resolve, reject)=>{
		for(i=0,c=that._d.sources.length;i<c;i++){

			if(that._d.sources[i]) {

				let source = that._d.sources[i];

				let findSourceInIntervals = that._d.intervals.findIndex(x=>x.id === source.id);
				if(findSourceInIntervals < 0) {
					
					let startOn = Math.round(Math.random()*that._d.config.intervals);

					global._s.log('. TWI: '+source.name+' - starting  in '+(startOn+options.interval)+' ms (interval:'+options.interval+',offset:'+startOn+')','I');
					that._d.intervals.push({
						name:source.name,
						id:source.id,
						interval:setInterval(
							()=>{
								that.setItems(source,options);
							},
							(options.interval+startOn)
						)

					});

				}

			}

		}

			resolve();

		});

	},

	// initialization promise 
	init:function(options) {

		var that = this;
		return new Promise((resolve,reject)=>{

			global._s.db.q(that._s.sources,[])
			.then(urls=>{

				that._d.sources = urls;
				that.client = new that.Twitter(that._tokens);

				that.interval(options);

				// Spool
				setInterval(()=>{

					if(that._d.spool.feeds.length>0){
						
						var promList = [],
							resultOfSpool = {
								resolved:0,
								rejected:[],
							},
							procList = [];

						global._s.log('. TWI: spool - '+that._d.spool.feeds.length+' items in queue' ,'D');

						let spoolCount = that._d.spool.feeds.length;
						if(spoolCount > that._d.spool.config.limit) {
							spoolCount = that._d.spool.config.limit;
						}

						for(let spoolI = 0; spoolI<spoolCount; spoolI++){

							let spooledItem = that._d.spool.feeds[spoolI];

							promList.push(new Promise((res,rej)=>{

								global._s.db.q(that._s.crcItem,[spooledItem._crc,spooledItem._crc])
									.then(resultOfCRCCheck=>{

										if(resultOfCRCCheck.length < 1){

											global._s.db.q(that._s.insertNewsItem,spooledItem)
												.then(resultOfInsert=>{

													resultOfSpool.resolved++;

													let isSpooled = that._d.spool.feeds.findIndex(x=> x._crc === spooledItem._crc);
													if(isSpooled > -1){

														that._d.spool.feeds.splice(isSpooled,1);
													}

													res();

												})
												.catch(e=>{

													// console.log('oops');
													// // console.log(e);
													// console.log(spooledItem);
													spooledItem.err = e;
													resultOfSpool.rejected.push(spooledItem);
													res();

												});

										} else {

											resultOfSpool.rejected.push(spooledItem);
											res();

										}

									})
									.catch(e=>{

										spooledItem.err = e;
										resultOfSpool.rejected.push(spooledItem);
										res();

									});

							}));

						}

						Promise.all(promList)
							.then(()=>{

								if(resultOfSpool.rejected.length > 0) {

									global._s.log('x TWI: spool - items rejected: '+resultOfSpool.rejected.length,'E');

									let errorCount = 0,
										errors = [];

									resultOfSpool.rejected.forEach(rejectedItem=>{											

										if(rejectedItem.err){

											errorCount++;
											errors.push(rejectedItem);
											
											let idxOfReject = that._d.spool.feeds.findIndex(x=>x._crc === rejectedItem._crc);
											if(idxOfReject > -1){

												that._d.spool.feeds.splice(idxOfReject,1);

											}

										}

									});
									if(errors.length > 0){

										global._s.log('x TWI: spool - errored '+errors.length+' times.','E');
										errors.forEach(err=>{

											global._s.log(err.err,'E',true);

										});

									}

								}
								if(resultOfSpool.resolved>0){

									global._s.log('✔ TWI: spool - inserted '+resultOfSpool.resolved+' items.','D');

								}

							})
							.catch(e=>{

								global._s.log('x TWI: error processing spool','E');
								global._s.log(e,'E',true);

							});

					}

				},that._d.spool.config.interval);				
				global._s.log('. TWI: spool - interval started ('+that._d.spool.config.interval+' ms)','I');

				setInterval(()=>{

					global._s.db.q(that._s.sources,[])
					.then(newURLs=>{

						for(var u=0,q=that._d.sources.length;u<q;u++){

							if(that._d.sources[u]) {

								let idx = newURLs.findIndex(x => x.id === that._d.sources[u].id);
								if(idx<0) {

									let name = that._d.sources[u].name;
									that._d.sources.splice(u,1);

									global._s.log('✔ TWI: source removed: '+name,'D');

								}

							}

						}

						for(var u=0,q=that._d.intervals.length;u<q;u++){

							if(that._d.intervals[u]) {

								let idx = newURLs.findIndex(x => x.id === that._d.intervals[u].id);
								if(idx<0) {

									let name = that._d.intervals[u].name;

									clearInterval(that._d.intervals[u].interval);
									that._d.intervals.splice(u,1);

									global._s.log('✔ TWI: interval removed: '+name,'D');

								}

							}

						}

						newURLs.forEach(s=>{

							let idx = that._d.sources.find(x=> x.id === s.id);
							if(!idx) {

								global._s.log('✔ TWI: source added: '+s.name,'D');
								that._d.sources.push(s);

							}

						});
						that.interval(options);

					})
					.catch(e=>{

						global._s.log('! TWI: error fetching sources from database','E');
						global._s.log(e,'E',true);

					});

				},that._d.config.sources);
				global._s.log('. TWI: sources update interval started','I');

				resolve();

			})
			.catch(e=>{

				reject(e);

			});

		}); // promise

	}

};