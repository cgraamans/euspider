'use strict';

module.exports = {

	// SQL
	_s: {

		crcItem:'SELECT _crc FROM `news.items` WHERE _crc = ? AND _source_id = ? UNION SELECT _crc FROM `news.item.archive` WHERE _crc = ? AND _source_id = ?',
		sources:'SELECT id, url_rss AS url, name, stub FROM `news.sources` WHERE isActive = 1 AND url_rss IS NOT NULL ORDER BY id DESC',
		insertNewsItem:'INSERT INTO `news.items` SET ?',
		updateSourceDT:'UPDATE `news.sources` SET _dt_update = ? WHERE id = ?',

	},

	// config variables for internal processing
	_d:{

		intervals:[],
		config:{
			initial:60000, // feed's initial starting time variable
			sources:360000, // source starting time variable
		},

		spool:{

			feeds:[],
			config:{
				limit:1, // number of inserts
				interval:300, // number of seconds between inserts
			},

		},

	},

	// - fetch feeds
	fetch:function(feed){

		let that = this;
		return new Promise((resolve,reject)=>{

			let feedParser = require('feedparser-promised');

			global._s.log('. RSS: '+feed.name+' - fetching feed','D');
			feedParser.parse({uri:feed.url},{feedurl:feed.url,resume_saxerror:true})
				.then(items=> {

					feed.items = items;
					global._s.log('✔ RSS: '+feed.name+' - fetched feed','I'); 
					resolve(feed);

				})
				.catch(e=>{

					reject(e);

				});

		});

	},

	// - remove all duplicates from item lists
	processFeed:function(feed){

		var that = this;
		return new Promise((resolve,reject)=>{

			let crcRun = [],
				readyList = [],
				rejectList = [];

			for(var t=0,w=feed.items.length;t<w;t++){

				let item = feed.items[t];
				if((item.title) && (item.link)) {

					let returnItem = {
						
						_source_id:feed.id,
						_date:global._s.worker.dt.yyyyMMDD(),
						_dt:global._s.worker.dt.unixtime(),

						title:item.title,
						link:item.link,
					
					};

					if(item.description){
						returnItem.description = item.description	
					}
					
					if(item.author){
						returnItem.author = item.author;	
					}

					if(item.summary){
						returnItem.summary = global._s.worker.html.decode(item.summary);
					}

					if(item.image){
					
						if(item.image.url) {

							returnItem.img = item.image.url;
						
						}

					}

					if(item.pubDate){

						returnItem.pubDate = item.pubDate;

						let itemDate = new Date(item.pubDate);
						if(itemDate){

							let itemUnixDT = Math.round(itemDate.getTime()/1000);
							if(itemUnixDT){

								if((itemUnixDT.toString().length === 10) && (typeof itemUnixDT === 'number')){

									if(itemUnixDT < item._dt){

										item._dt = itemUnixDT;

									}

								}

							}

						}

					} else {

						returnItem.pubDate = global._s.worker.dt.yyyyMMDD();

					}

					crcRun.push(new Promise((res,rej)=>{

						global._s.auth.encrypt(returnItem.title+'/'+returnItem._source_id+'/'+returnItem.link,true)
							.then(crcToken=>{

								returnItem._crc = crcToken;

								global._s.db.q(that._s.crcItem,[returnItem._crc,returnItem._source_id,returnItem._crc,returnItem._source_id])
									.then(crcCopy=>{

										if(crcCopy.length===0){

											readyList.push(returnItem);
											res();

										} else {

											rejectList.push(returnItem);
											res();

										}
										

									})
									.catch(e=>{

										returnItem.err = e;
										rejectList.push(returnItem);
										
										res();

									});

						})
						.catch(e=>{

							returnItem.err = e;
							rejectList.push(returnItem);
							res();


						});

					}));

				}

			}
			Promise.all(crcRun)
				.then(crcEdFeed=>{
				
					resolve({
						resolve:readyList,
						reject:rejectList,
					});
				
				})
				.catch(e=>{
				
					reject(e);
				
				});

		});

	},

	// main interval trigger
	interval:function(source){

		var that = this;
			that.fetch(source)
			.then(assembledFeed=>{

				if(assembledFeed) {

					that.processFeed(assembledFeed)
						.then(processedFeed=>{

							if(processedFeed.reject.length>0){

								global._s.log('. RSS: '+source.name +' - process feed - items rejected: '+processedFeed.reject.length,'D');

								let errorCount = 0,
									errors = [];

								processedFeed.reject.forEach(rejectedItem=>{											

									if(rejectedItem.err){

										errorCount++;
										errors.push(rejectedItem);

									}

								});
								if(errors.length > 0) {
								
									global._s.log('x RSS: '+source.name +' - process feed - errors: '+errors.length,'E');

									errors.forEach(err=>{

										global._s.log(err,'E');

									});
								
								}

							}

							if(processedFeed.resolve.length>0){

								let countSpooled = 0,
									countRejected = 0;
								processedFeed.resolve.forEach(newItem=>{

									let isSpooled = that._d.spool.feeds.findIndex(x=>x._crc === newItem._crc);
									if(isSpooled < 0){

										that._d.spool.feeds.push(newItem);
										countSpooled++;

									} else {

										countRejected++;

									}

								});


								if(countSpooled>0){

									global._s.log('✔ RSS: '+source.name+' - interval - items added to spool: '+countSpooled,'D');	

								}

								if(countRejected>0){

									global._s.log('! RSS: '+source.name+' - interval - duplicates in spool: '+countRejected,'E');	

								}

							}


						})
						.catch(e=>{

							global._s.log('x RSS: '+source.name+' - interval - processing failed','E');
							global._s.log(e,'E');

						});

				}

			})
			.catch(e=>{

				global._s.log('x RSS: '+source.name+' - interval - fetch error ','E');
				global._s.log(e,'E');

			});

	},

	// initialization promise 
	init:function(options) {

		var that = this;
		return new Promise((resolve,reject)=>{

			global._s.db.q(that._s.sources,[])
				.then(function(urls){

					// RSS Start
					for(var a=0,c=urls.length;a<c;a++){

						let startOn = Math.round(Math.random()*that._d.config.initial),
							source = urls[a];
						global._s.log('. RSS: '+source.name+' - starting in '+(startOn+options.interval)+' ms (interval:'+options.interval+',offset:'+startOn+')','I');

						that._d.intervals.push({
							id:source.id,
							name:source.name,
							interval:setInterval(()=>{
								that.interval(source);
							},startOn+options.interval)
						});

					}

					// Spool
					setInterval(()=>{

						if(that._d.spool.feeds.length>0){
							
							var promList = [],
								resultOfSpool = {
									resolved:0,
									rejected:[],
								};

							global._s.log('. RSS: spool - '+that._d.spool.feeds.length+' items in queue' ,'I');

							let spoolCount = that._d.spool.feeds.length;
							if(spoolCount > that._d.spool.config.limit) {
								spoolCount = that._d.spool.config.limit;
							}

							for(let spoolI = 0; spoolI<spoolCount; spoolI++){

								let spooledItem = Object.assign({},that._d.spool.feeds[spoolI]);
								that._d.spool.feeds = that._d.spool.feeds.filter(x=>x._crc !== spooledItem._crc);

								promList.push(new Promise((res,rej)=>{

									global._s.db.q(that._s.crcItem,[spooledItem._crc,spooledItem._source_id,spooledItem._crc,spooledItem._source_id])
										.then(resultOfCRCCheck=>{

											if(resultOfCRCCheck.length<1){

												global._s.db.q(that._s.insertNewsItem,spooledItem)
													.then(resultOfInsert=>{

														resultOfSpool.resolved++;
														res();

													})
													.catch(e=>{

														spooledItem.err = e;
														resultOfSpool.rejected.push(spooledItem);
														res();

													});

											} else {

												resultOfSpool.rejected.push(spooledItem);
												res();

											}

										})
										.catch(e=>{

											spooledItem.err = e;
											resultOfSpool.rejected.push(spooledItem);
											res();

										});

								}));

							}

							Promise.all(promList)
								.then(()=>{

									if(resultOfSpool.rejected.length > 0) {

										global._s.log('x RSS: spool - items rejected: '+resultOfSpool.rejected.length,'E');

										let errorCount = 0,
											errors = [];

										resultOfSpool.rejected.forEach(rejectedItem=>{											

											if(rejectedItem.err){

												errorCount++;
												errors.push(rejectedItem);
												
												let idxOfReject = that._d.spool.feeds.findIndex(x=>x._crc === rejectedItem._crc);
												if(idxOfReject > -1){

													that._d.spool.feeds.splice(idxOfReject,1);

												}

											}

										});
										if(errors.length > 0){

											global._s.log('x RSS: spool - errored '+errors.length+' times.','E');
											errors.forEach(err=>{

												global._s.log(err,'E');

											});

										}

									}
									if(resultOfSpool.resolved>0){

										global._s.log('✔ RSS: spool - inserted '+resultOfSpool.resolved+' items.','I');

									}

								})
								.catch(e=>{

									global._s.log('x RSS: error processing spool','E');
									global._s.log(e,'E');

								});

						}

					},that._d.spool.config.interval);				
					global._s.log('. RSS: spool - interval started','I');

					// Source isActive check
					setInterval(()=>{

						global._s.db.q(that._s.sources,[])
							.then(function(newURLs){

								for(var u=0,q=that._d.intervals.length;u<q;u++){

									if(that._d.intervals[u]) {

										let idx = newURLs.findIndex(x => x.id === that._d.intervals[u].id);
										if(idx<0) {

											if(that._d.intervals[u].interval) {
												
												global._s.log('! RSS: sources update - stopped  '+that._d.intervals[u].name,'I');

												clearInterval(that._d.intervals[u].interval);
												
												that._d.intervals.splice(u,1);

											}

										}

									}

								}

								newURLs.forEach(s=>{

									let idx = that._d.intervals.find(x=> x.id === s.id);
									if(!idx) {

										let startOn = Math.round(Math.random()*that._d.config.initial);
										global._s.log('. RSS: '+s.name+' - starting  in '+(startOn+options.interval)+' ms (interval:'+options.interval+',offset:'+startOn+')','I');

										that._d.intervals.push({
											id:s.id,
											name:s.name,
											interval:setInterval(()=>{
												that.interval(s);
											},startOn+options.interval)
										});

									}

								});
								global._s.log('✔ RSS: sources refreshed','I');

							})
							.catch(e=>{

								global._s.log('x RSS: sources update - error fetching sources from database','E');
								global._s.log(e,'E');

							});

					},that._d.config.sources);
					global._s.log('. RSS: sources refresh - interval started','I');

					// resolve();


				})
				.catch(e=>{

					reject(e);

				});

		});

	}

};