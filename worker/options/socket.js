'use strict';

module.exports = {

	host:'http://localhost:9001',
	auth:{
		apiId:process.env.EUSPIDER_WORKER_ID,
		apiToken:process.env.EUSPIDER_WORKER_TOKEN,
	},
	// isConnected:false,
	// retries:100,
	// retryTimeout:500,
	// connectionPoll:6000, // ms

};
