module.exports = {

	// Where are the instance files
	location:'../instances/',

	// what are their filenames and requirements
	list:[
	
		{
			
			name:'rss',
			interval:60000,
		
		},
		
		{
			
			name:'twitter',
			interval:120000,
		
		},
		
		{
			
			name:'twitter.old',
			interval:120000,
		
		},
		
		{ 

			name:'archive',
			interval:10000,

		},

	]

};