'use strict';

global._s = {

	path:require('path'),

	io:require('socket.io-client'),

	worker:require('./lib/worker'),
	auth:require('./lib/auth'),
	db:require('./lib/db'),
	log:require('./lib/log'),

	opt:{
		instances:require('./options/instances'),
		socket:require('./options/socket'),
		db:require('./options/db'),
	},
	crypto:{
		jwt:require('jsonwebtoken'),
		SHA256:require('crypto-js/sha256'),
	},
	intervals:[],
	loglevel:'I',

};


global._s.log('. Starting App ('+Date.now()+')','I');
global._s.appDir = global._s.path.dirname(require.main.filename);

// prime database connection
try {

	global._s.db.createConnection(global._s.opt.db.settings)
		.then(function(){

			global._s.log('✔ MySQL Connection','I');

		})
		.catch(function(err){

			if(err){
				throw err;	
			} else {

			}
			

		});

} catch(e) {

	global._s.log('! Error Creating MySQL Pool','E');
	global._s.log(e,'E',true);
	process.exit(1);

}

// prime api connection
try {

	let args = process.argv.slice(2),
		runInstances = global._s.opt.instances.list;
		
	if(args.length > 0){
		var argv = require('minimist')(args);
		let instances = [];

		if(argv.start){
			if(argv.start.indexOf(',') > -1){
				let protoInstances = argv.start.split(',');
				if(protoInstances){
					protoInstances.forEach(instanceFromArgs=>{
						let instancePos = global._s.opt.instances.list.findIndex(x=>x.name === instanceFromArgs);
						if(instancePos > -1){
							instances.push(global._s.opt.instances.list[instancePos])
						}
					});
				}
			} else {
				let instancePos = global._s.opt.instances.list.findIndex(x=>x.name === argv.start);
				if(instancePos > -1){
					instances.push(global._s.opt.instances.list[instancePos])
				}		
			}
		}
		if(instances.length>0){
			runInstances = instances;
		}

	}

	global._s.socket = global._s.io(global._s.opt.socket.host);

	global._s.log('✔ Connected To API','I');

	global._s.socket.on('init',initData=>{

		global._s.log('✔ Initialization of socket received','D');

		if((initData.ok === true) && (initData.secret.length === 64)) {

			global._s.opt.socket.secret = initData.secret;
			global._s.log('✔ Sending Auth Request','D');

			global._s.worker.emit('auth',global._s.opt.socket.auth)
				.catch(e=>{

					throw e;
				
				});

			global._s.opt.auth = {

				isConnected:false

			};
			global._s.socket.on('auth',authData=>{

				global._s.log('✔ Auth reply received','D');

				global._s.auth.decode(authData)
					.then(msg=>{

						if (msg.ok === true) {

							global._s.log('✔ Authorized with API','I');
							global._s.opt.auth.isConnected = true;

							var p = [];
							for(var a=0,c=runInstances.length;a<c;a++){

								p.push(global._s.worker.instance(runInstances[a]));

							}
							Promise.all(p)
								.then(intervals=>{

									intervals.forEach(interval=>{
										
										if(interval){
											
											global._s.log('✔ Started worker '+interval.name,'I');
											global._s.worker.emit('log',{msg:'Started',actor:interval.name});

											global._s.intervals.push(interval);
										
										}
										
									});

								})
								.catch(e=>{

									throw e;

								});

						} else {

							throw msg;

						}

					})
					.catch(e=>{

						throw e;
											
					});

			});

		}

	});

} catch(e) {

	global._s.log('! Error initializing socket connection...','E');
	global._s.log(e,'E',true);
	process.exit(1);

}