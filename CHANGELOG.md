# EUSPIDER 0.10.1 #

## SQL Schema Updates: ##

	ALTER TABLE `user.tokens` ADD COLUMN `id` INT(255) NOT NULL AUTO_INCREMENT FIRST, DROP PRIMARY KEY, ADD PRIMARY KEY (`id`);
	ALTER TABLE `news.sources` ADD COLUMN `type` VARCHAR(2) NOT NULL DEFAULT 'O' AFTER `isActive`, DROP COLUMN `isOfficial`, DROP COLUMN `isIndividual`;
	ALTER TABLE `news.sources` ADD COLUMN `_language` VARCHAR(3) NULL DEFAULT 'EN' AFTER `_country`;
	ALTER TABLE `news.sources` CHANGE COLUMN `_country` `country` VARCHAR(3) NULL DEFAULT 'BE' AFTER `url_twitter`, CHANGE COLUMN `_language` `language` VARCHAR(3) NULL DEFAULT 'EN' AFTER `country`;

	UPDATE `euspider`.`news.sources` SET `url_rss`=NULL, `url_twitter`='EU_Eurostat' WHERE `stub`='eurostat.eu';
	UPDATE `euspider`.`news.sources` SET `url_rss`=NULL, `url_twitter`='EFSA_EU' WHERE `stub`='efsa.eu';	
	UPDATE `euspider`.`news.sources` SET `type`='N' WHERE stub IN ('euractiv','euronews',"euronews","bbc","guardian.uk","thelocal.se","thelocal.de","thelocal.fr","thelocal.es","thelocal.dk","dw");
	UPDATE `euspider`.`news.sources` SET `type`='NP' WHERE stub = 'politico.eu';
	UPDATE `euspider`.`news.sources` SET `type`='T' WHERE stub = 'ecfr';
	UPDATE `euspider`.`news.sources` SET `url_rss`=NULL, `url_twitter`='EU_EDPS' WHERE `stub`='edps.eu';
	UPDATE `euspider`.`news.sources` SET isActive = 0 WHERE stub IN ("junckereu","ansip_eu","FedericaMog","TimmermansEU","janemorrice","jyrkikatainen","MarosSefcovic","VDombrovskis","Moedas","MalmstromEU","cert");

	INSERT INTO `euspider`.`news.sources` (`name`, `stub`, `isActive`, `type`, `url_rss`, `url_twitter`, `country`, `language`) VALUES ('EUobserver', 'euobs', '1', 'N', null, 'euobs', 'BE', 'EN');
	INSERT INTO `euspider`.`news.sources` (`name`, `stub`, `isActive`, `type`, `url_rss`, `url_twitter`, `country`, `language`) VALUES ('CNN Europe', 'cnn.us', '1', 'N', 'http://rss.cnn.com/rss/edition_europe.rss', null, 'US', 'EN');
	INSERT INTO `euspider`.`news.sources` (`name`, `stub`, `isActive`, `type`, `url_rss`, `url_twitter`, `country`, `language`) VALUES ('Europa Newsroom', 'europa.newsroom', '1', 'N', 'https://europa.eu/newsroom/press-releases.xml_en', null, 'BE', 'EN');
	INSERT INTO `euspider`.`news.sources` (`name`, `stub`, `isActive`, `type`, `url_rss`, `url_twitter`, `country`, `language`) VALUES ('EU Council Press', 'euCouncilPress.eu', '1', 'O', null, 'euCouncilPress', 'BE', 'EN');
	INSERT INTO `euspider`.`news.sources` (`name`, `stub`, `isActive`, `type`, `url_rss`, `url_twitter`, `country`, `language`) VALUES
	('Carnegie Europe', 'CarnegieEurope.uk', '1', 'T', NULL, 'Carnegie_Europe', 'UK', 'EN'),
	('Centre for European Reform', 'cer.uk', '1', 'T', NULL, 'cer_eu', 'UK', 'EN'),
	('EIAS', 'EIAS.be', '1', 'T', NULL, 'eiasbrussels', 'BE', 'EN'),
	('European Institute of Public Administration', 'EIPA.nl', '1', 'T', NULL, 'eu_eipa', 'NL', 'EN'),
	('ESI', 'ESI.be', '1', 'T', NULL, 'esi_eu', 'BE', 'EN'),
	('European Policy Centre', 'EPC.be', '1', 'T', NULL, 'epc_eu', 'BE', 'EN'),
	('Foundation for European Progressive Studies', 'FEPS.be', '1', 'T', NULL, 'FEPS_Europe', 'BE', 'EN'),
	('Friends of Europe', 'FriendsofEurope.be', '1', 'T', NULL, 'FriendsofEurope', 'BE', 'EN'),
	('The Federal Trust', 'FedTrust.uk', '1', 'T', NULL, 'FriendsofEurope', 'UK', 'EN'),
	('Foreign Policy Centre', 'FPC.uk', '1', 'T', NULL, 'fpcthinktank', 'UK', 'EN'),
	('HCSS', 'HCSS.nl', '1', 'T', NULL, 'hcssnl', 'NL', 'EN'),
	('ISDP', 'ISDP.se', '1', 'T', NULL, 'isdp_sweden', 'SE', 'EN'),
	('Crisis Group', 'CrisisGroup.eu', '1', 'T', NULL, 'CrisisGroup', 'EU', 'EN'),
	('Lisbon Council for Economic Competitiveness and Social Renewal', 'LisbonCouncil.eu', '1', 'T', NULL, 'lisboncouncil', 'BE', 'EN'),
	('Overseas Development Institute', 'ODIdev.eu', '1', 'T', NULL, 'ODIdev', 'EU', 'EN'),
	('Policy Network', 'policynetwork.eu', '1', 'T', NULL, 'policynetwork', 'UK', 'EN');
	INSERT INTO `euspider`.`news.sources` (`name`, `stub`, `isActive`, `type`, `url_rss`, `url_twitter`, `country`, `language`) VALUES
	('Bruegel', 'Bruegel.be', '1', 'T', 'http://bruegel.org/feed/', NULL, 'BE', 'EN'),
	('CEPS', 'CEPS.be', '1', 'T', 'https://www.ceps.eu/rss1.php', NULL, 'BE', 'EN'),
	('Chattam House', 'ChattamHouse.uk', '1', 'T', 'http://feeds.feedburner.com/ChathamHouse-WhatsNew', NULL, 'UK', 'EN'),
	('European Centre for Development Policy Management', 'ECDPM.uk', '1', 'T', 'http://ecdpm.org/feed/ecdpm-all.xml', NULL, 'NL', 'EN'),
	('European Institute for Security Studies', 'EISS.eu', '1', 'O', 'https://www.iss.europa.eu/euiss-rss-view.xml', NULL, 'EU', 'EN'),
	('International Peace Institute', 'IPI.eu', '1', 'T', 'https://www.ipinst.org/feed', NULL, 'US', 'EN');

	UPDATE `news.sources` SET stub = 'newsroom.eu', url_rss = 'https://europa.eu/newsroom/press-releases.xml' where stub = 'europa.newsroom';

# EUSPIDER 0.10.2 #

- New rss and twitter datetime function

## SQL Schema Updates: ##

	INSERT INTO `euspider`.`news.sources` (`name`, `stub`, `isActive`, `type`, `url_rss`, `url_twitter`, `country`, `language`) VALUES 
	('The Independent', 'independent.uk', '1', 'N', 'http://www.independent.co.uk/topic/EU/rss', NULL, 'UK', 'EN'),
	('Der Spiegel', 'spiegel.de', '1', 'N', 'http://www.spiegel.de/international/index.rss', NULL, 'DE', 'EN'),
	('DutchNews', 'dutchnews.nl', '1', 'N', 'http://www.dutchnews.nl/feed/?news', NULL, 'NL', 'EN'),
	('France24', 'france24.fr', '1', 'N', 'http://www.france24.com/en/europe/rss', NULL, 'FR', 'EN'),
	('The Florentine', 'florentine.it', '1', 'N', 'http://www.theflorentine.net/feed/', NULL, 'IT', 'EN'),
	('BE News', 'benews.it', '1', 'N', 'http://www.benewsberlin.com/en_US/feed/', NULL, 'DE', 'EN'),
	('Warsaw Business Journal', 'wbj.pl', '1', 'N', 'http://wbj.pl/feed/', NULL, 'PL', 'EN'),
	('Prague Daily Monitor', 'praguedailymonitor.cz', '1', 'N', 'http://feeds2.feedburner.com/PragueDailyMonitor', NULL, 'CZ', 'EN'),
	('Independent.ie', 'independent.ie', '1', 'N', 'https://www.independent.ie/world-news/europe/rss/', NULL, 'IE', 'EN'),
	('Irish Times', 'irishtimes.ie', '1', 'N', 'https://www.independent.ie/world-news/europe/rss/', NULL, 'IE', 'EN'),
	('CPH Post', 'cph-post.cz', '1', 'N', 'http://cphpost.dk/rss-feed/', NULL, 'DK', 'EN'),
	('Lodz Post', 'lodz-post.cz', '1', 'N', 'http://lodzpost.com/feed/', NULL, 'PL', 'EN');
		
	UPDATE `euspider`.`news.sources` SET logo = 'independent.png' WHERE stub = 'independent.uk';
	UPDATE `euspider`.`news.sources` SET logo = 'spiegel.de.png' WHERE stub = 'spiegel.de';
	UPDATE `euspider`.`news.sources` SET logo = 'cnn.png' WHERE stub = 'cnn.us';
	UPDATE `euspider`.`news.sources` SET logo = 'cz.pdm.png' WHERE stub = 'praguedailymonitor.cz';
	UPDATE `euspider`.`news.sources` SET logo = 'f24.png' WHERE stub = 'france24.fr';

	INSERT INTO `euspider`.`news.sources` (`name`, `stub`, `isActive`, `type`, `url_rss`, `url_twitter`, `country`, `language`) VALUES ('European Parliament Press Service', 'EuroParlPress.eu', '1', 'O', NULL, 'EuroParlPress', 'BE', 'EN');

	update `news.sources` set country = 'EU' where country = '';
	update `news.sources` set country = 'EU' where country IS NULL;

	update `news.sources` set country = 'EU' where stub in ('council.eu','cor.eu');
	update `news.sources` set country = 'BE' where country = 'EU' AND `type` != 'O';
	INSERT INTO `euspider`.`news.sources` (`name`, `stub`, `isActive`, `type`, `url_rss`, `url_twitter`, `country`, `language`) VALUES ('CERN', 'cern.eu', '1', 'NT', 'https://press.cern/feed', NULL, 'FR', 'EN');

# EUSPIDER 0.10.3 #

## SQL Schema Updates: ##

INSERT INTO `euspider`.`news.sources` (`name`, `stub`, `type`, `url_rss`) VALUES ('The Baltic Times', 'baltictimes.eu', 'N', 'http://feeds.feedburner.com/TheBalticTimesNews');
INSERT INTO `euspider`.`news.sources` (`name`, `stub`, `type`, `url_rss`) VALUES ('LabioTech', 'labiotech.eu', 'NT', 'https://labiotech.eu/feed/');
INSERT INTO `euspider`.`news.sources` (`name`, `stub`, `type`, `url_rss`) VALUES ('The Economist', 'economist.com', 'N', 'http://www.economist.com/sections/europe/rss.xml');
INSERT INTO `euspider`.`news.sources` (`name`, `stub`, `type`, `url_rss`,`country`) VALUES ('ANSA', 'ansa.it', 'N', 'https://www.ansa.it/english/english_rss.xml','IT');
INSERT INTO `euspider`.`news.sources` (`name`, `stub`, `type`, `url_rss`) VALUES ('European Western Balkans', 'europeanwesternbalkans.eu', 'N', 'https://europeanwesternbalkans.com/feed/');
INSERT INTO `euspider`.`news.sources` (`name`, `stub`, `type`, `url_twitter`) VALUES ('Pact for Europe', 'pactforeu.eu', 'T', 'PactforEurope');
INSERT INTO `euspider`.`news.sources` (`name`, `stub`, `type`, `url_rss`, `logo`) VALUES ('The Local - IT', 'thelocal.it', 'N', 'https://www.thelocal.it/feeds/rss.php', 'local.png');

UPDATE `news.sources` SET logo='labiotech_eu.jpg' WHERE stub='labiotech.eu';
UPDATE `news.sources` SET logo='baltictimes.png' WHERE stub='baltictimes.eu';
UPDATE `news.sources` SET logo='ansa.png' WHERE stub='ansa.it';
UPDATE `news.sources` SET logo='irishtimes.png' WHERE stub='irishtimes.ie';
UPDATE `news.sources` SET logo='europeanwesternbalkans.jpg' WHERE stub='europeanwesternbalkans.eu';
UPDATE `news.sources` SET logo='economist.jpg' WHERE stub='economist.com';
UPDATE `news.sources` SET logo='cern.jpg' WHERE stub='cern.eu';
UPDATE `news.sources` SET logo='dailyfinland.jpg' WHERE stub='dailyfinland.fi';
UPDATE `news.sources` SET logo='cphpost.png' WHERE stub='cph-post.cz';
UPDATE `news.sources` SET logo='wbj.jpg' WHERE stub='wbj.pl';
UPDATE `news.sources` SET logo='dn.jpg' WHERE stub='dutchnews.nl';
UPDATE `news.sources` SET logo='bruegel.PNG' WHERE stub='Bruegel.be';
UPDATE `news.sources` SET logo='independent.ie.jpg' WHERE stub='independent.ie';



# Notes #

## Source Types ##

The types for sources are:

	T : Thinktank
	N : News
	NP: News - Politics
	NS: News - Sports
	NT: News - Tech
	O : Agency/Organization
