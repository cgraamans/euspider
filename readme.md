# EUSpider 0.9 #

EUspider is a website made to retrieve or 'spider' news from various Eu based news sources and present that to users.

The website aims to get users to rate news on accuracy and news-worthiness by presenting various news from different news sources across a range of language areas within and about the EU.

Users are encouraged to vote on the basis of newsworthiness and reliability of the articles as well as discuss the news in an open-ended and respectful forum.

## Set Up ##

This application is meant as open source project, available for all to review and use. It is based on the nspider application, using the same back-end, front-end and worker setup.

See [the nspider project](http://bitbucket.org/cgraamans/nspider) for setup instructions.