'use strict';

// AngularJS & Polyfills
var angular = require('angular');
require('babel-polyfill');

// AngularJS MODULES
require('angular-route');
require('angular-animate');
require('angular-touch');
require('angular-sanitize');
require('ng-showdown');
require('angular-scroll');

// AngularJS Services
require('./services/socket');
require('./services/storage');

// INIT
var app = angular.module('nspider', ['ngRoute','ngAnimate','socketService','ngTouch','storageService','ngSanitize','ng-showdown','duScroll']);
app.config(require('./options/routes'));

app.run(($rootScope,ls)=>{

	$rootScope.current = {

		log:[],
		user:{},
		overlay:{
			auth:false,
		},
		navs:{
			locales:false,
			user:false,
			dark:false,
		},		
		news:{
			updates:{
				items:0,
				reload:false,
			},
			params:{
				orderBy:'votesUp',
				sourceType:false,
			},

		},

	};

	let isDark = ls.get('isDark');
	if(isDark && isDark.isDark) {
		$rootScope.current.navs.dark = true;
	}
	$rootScope.$on('theme-switch', function() {
		$rootScope.current.navs.dark = !$rootScope.current.navs.dark;
		ls.set('isDark',$rootScope.current.navs.dark);
	});

});

require('./directives/nav.header');
require('./directives/nav.header.user');
require('./directives/nav.votes');
require('./directives/nav.overlay.auth');

require('./directives/popover.reddit');
require('./directives/popover.orderby');
require('./directives/popover.sourcetype');

require('./directives/comments');
require('./directives/comment.box');
require('./directives/item');
require('./directives/source');

require('./directives/cookie.warning');

require('./controllers');

// BOOTSTRAP FOR CORDOVA
if (window.cordova) {

    document.addEventListener('deviceready', function onDeviceReady() {
        angular.bootstrap(document, ['nspider']);
    }, false);

} else {

    angular.bootstrap(document, ['nspider']);

}