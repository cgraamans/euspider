//
// Socket Client Configuration
//

'use strict';

module.exports = {
	// Socket.IO Server URI
	uri:'http://tethys:9001',
	
	// PREFIX FOR EVERY EMIT
	prefix:'',

	// Verify the token on login every x ms
	verifyTimer:60000,

};
