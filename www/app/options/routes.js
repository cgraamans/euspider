'use strict';

module.exports = function($routeProvider,$locationProvider) {

	$routeProvider

		// MAIN
		.when('/', {
			controller: 'NewsController',
			templateUrl: 'views/news.html'
		})

		.when('/n/:params?', {
			controller: 'NewsController',
			templateUrl: 'views/news.html'
		})	

		.when('/i/:item/:params?', {
		  controller: 'ItemController',
		  templateUrl: 'views/item.html'
		})

		// USER
		.when('/u/:user/comments/:params?', {
			controller: 'UserCommentsController',
			templateUrl:'views/user.comments.html'
		})

		.when('/u/:user/saves/:type?/:params?', {
			controller: 'UserSavesController',
			templateUrl:'views/user.saves.html'
		})

		.when('/u/:user/settings/:params?', {
			controller: 'UserSettingsController',
			templateUrl:'views/user.settings.html'
		})

		.when('/u/:user/sources/:params?', {
			controller: 'UserSourcesController',
			templateUrl:'views/user.sources.html'
		})

		.when('/u/:user/:params?', {
			controller: 'UserCommentsController',
			templateUrl:'views/user.comments.html'
		})

		.when('/logout', {
			controller: 'UserLogoutController',
			templateUrl: 'views/news.html'
		})

		// CONFIG
		.otherwise({redirectTo:'/'});

	$locationProvider.html5Mode(true);

};
