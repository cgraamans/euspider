'use strict';

require('angular-socket-io');

let io = require('socket.io-client'),
opt = require('../options/sockets.js');

var app = require('angular').module('socketService',['btford.socket-io']);	
app.factory('socket', function (socketFactory,ls,$rootScope) {

	let obj = {

		secret:false,

		_:{

			fn:{
				
				emit:(sock,data)=>{

					obj._.jwt.encode(obj.secret,data,(err, token)=>{

						if (err) {

							$rootScope.current.log.push({t:'e',e:err});
						
						} else {
						
							obj.s.emit(sock,token);
						
						}
						return;

					});

				},

			},
			cryptojs:require('crypto-js'),
			jwt:require('json-web-token'),

		},

		user:{

			data:()=>{

				let rtn = false,
					aD = ls.get('auth');

				if(aD) {
					rtn = aD.auth;
				}

				return rtn;

			},

			isLoggedIn:()=>{

				let rtn = false,
					aD = ls.get('auth');

				if(aD.auth) {

					if(aD.auth.apiId){

						rtn = true;

					}

				}
				return rtn;

			},

		},
		
		emit:(sock,data)=>{

			let rtn;
			if(obj.secret){
				
				rtn = obj._.fn.emit(sock,data);

			} else {

				let i = setInterval(()=>{

					if(obj.secret){
				
						rtn = obj._.fn.emit(sock,data);
						clearInterval(i);
				
					}

				},300);

			}
			return rtn;

		},

		decrypt:msg=>{

			return new Promise((resolve,reject)=>{

				obj._.jwt.decode(obj.secret,msg,(err, data)=>{

					if(err){

						$rootScope.current.log.push({t:'e',e:err});
						$rootScope.$apply();

						reject(err);
					
					} else {
						resolve(data);
					}

				});

			});

		},

	};

	obj._.io = io(opt.uri);
	obj.s = socketFactory({
		ioSocket: obj._.io,
	});

	obj.s.on('init',init=>{

		if(init.ok === true) {

			obj.secret = init.secret;
			$rootScope.current.log.push({t:'m',m:'✔ Authentication - Connection succesful.',dt:Math.round((new Date()).getTime()/1000)});

		} else {

			if(init.bans) {

				$rootScope.current.log.push({t:'b',m:'! Authentication - You are banned',d:init.bans,dt:Math.round((new Date()).getTime()/1000)});
				ls.destroy();

			}

		}
		$rootScope.$apply();

	});

	obj.s.on('auth',auth=>{

		obj._.jwt.decode(obj.secret,auth,(err, authData)=>{
				
				if(authData.ok === true) {

					if(authData.action) {
						$rootScope.current.log.push({t:'m',m:'. Authentication - Update.',dt:Math.round((new Date()).getTime()/1000)});						
					}

					if(authData.user){

						$rootScope.current.log.push({t:'m',m:'✔ Authentication - Logged in.',dt:Math.round((new Date()).getTime()/1000)});
						ls.set('auth',authData.user);
						$rootScope.current.overlay.auth = false;

					}

				} else {

					ls.destroy();
					$rootScope.current.log.push({t:'m',m:'. Authentication - Logged out.',dt:Math.round((new Date()).getTime()/1000)});

				}

			});

	});

	return obj;

});