'use strict';

require('angular-local-storage');

var app = require('angular');

app.module('storageService',['LocalStorageModule'])
.config(function (localStorageServiceProvider) {

        localStorageServiceProvider.setPrefix('nspider');

})
.factory('ls', function (localStorageService) {

	var rtnObj = {

        set: function(key,obj) {
            
            return localStorageService.set(key,JSON.stringify(obj));

        },

        get: function(key) {
            try {
                
                let val = localStorageService.get(key),
                    rtn = {};
                let parsed = JSON.parse(val);
                if(parsed){
                    rtn[key] = parsed;
                }
                return rtn;
            
            } catch (e) {

                return {e:e};

            }
            
        },
        
        remove: function(key) {

            return localStorageService.remove(key);

        },
        
        destroy: function() {

            return localStorageService.clearAll();

        }

    };


    if(!rtnObj.get('lang').lang){
        rtnObj.set('lang',{val:'en'});
    }

    return rtnObj;

});
