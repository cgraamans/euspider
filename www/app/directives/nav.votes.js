'use strict';

var angular = require('angular');
var app = angular.module('nspider');

app.directive('navVotes',function($rootScope,socket) {

	return {

		restrict: 'AE',
		scope: {

			votes:'=',
			vote:'=',
			item:'=',
			comment:'=',

		},
        templateUrl:'views/directives/nav.votes.html',

		link: function (scope) {

			scope.ctrl = {

				foldout:false,

			};

			$(function () {
				$('[data-toggle="tooltip"]').tooltip()
			})

			scope.vm = {

				isLoggedIn:()=>{

					return socket.user.isLoggedIn();

				},

				vote:(v)=>{

					let transmit = {
						user:socket.user.data(),
					};

					if(scope.item){
						transmit.item = scope.item;
					}

					if(scope.comment){
						transmit.comment = scope.comment;
					}

					if(v==='u'){

						transmit.vote = 1;
						if(scope.vote){
							if(scope.vote > 0){
								transmit.vote = 0;
							}
						}

					} else {

						transmit.vote = -1;
						if(scope.vote){
							if(scope.vote < 0){
								transmit.vote = 0;
							}
						}

					}

					socket.emit('user-vote',transmit);

				},

				votes:()=>{

					if(scope.votes > 1000000) {
						return (Math.floor(scope.votes/100000)/10) + ' m';
					}
					if(scope.votes > 100) {
						return (Math.floor(scope.votes/100)/10) + ' k';
					}
					if(scope.votes < -100) {
						return '-' + (Math.floor(scope.votes/100)/10) + ' k';
					}

					if(scope.votes < -1000000) {
						return '-' + (Math.floor(scope.votes/100000)/10) + ' m';
					}

					return scope.votes;

				}

			};

		}

	};

});
