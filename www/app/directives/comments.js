'use strict';

var angular = require('angular');
var app = angular.module('nspider');

app.directive('comments',()=>{

	return {

		restrict: 'E',
		replace: true,
		scope: {

			comments: '=',
			item:'=',
			children:'=',
			sort:'=',
			isStandalone:'=',
		
		},
		templateUrl: 'views/directives/comments.container.html',

	};

})
.directive('comment',($compile,$rootScope,socket)=>{

	return {
		restrict: 'E',
		replace: true,
		scope: {

			comment: '=',
			children:'=',
			sort:'=',
			item:'=',
			isStandalone:'=',

		},
		templateUrl: 'views/directives/comments.comment.html',
		link:(scope)=>{

			scope.ctrl = {

				show:{

					submenu:false,

					comment:false,
					edit:false,
					delete:false,
					reply:false,
					report:false,
					reported:true,

				},

				text:{
					size:'lg'
				},

				comments:[],
				children:[],

			};

			if(scope.comment.reported<3){
				scope.ctrl.show.comment = true;
			}
			if(scope.comment.txt.length>120){
				scope.ctrl.text.size='med';
			}
			if(scope.comment.txt.length>320){
				scope.ctrl.text.size='small';
			}

			scope.vm = {

				threads:()=>{

					let pRtn = {

						parent:scope.comment.stub,
					
					};

					if(socket.user.isLoggedIn()){
						pRtn.user = socket.user.data();
					}

					socket.emit('comments',pRtn);

				},

				isLoggedIn:()=>{

					return socket.user.isLoggedIn();

				},

				auth:()=>{

					let rtn = -1;
					if(socket.user.isLoggedIn() === true){

						let data = socket.user.data();
						rtn = 0;
						if(data.auth){

							rtn = data.auth;
						
						}

					}
					return rtn;

				},

				date:(dt)=>{

					let now = Math.round((new Date()).getTime()/1000);
					let elapsed = now - dt;

					if(elapsed < 0){

						return '0 s';

					}

					if(elapsed < 60) {

						return elapsed+' s';

					}
					if(elapsed < 3600) {

						return Math.round((elapsed/60)*10)/10+' m';

					}

					if(elapsed < 86400) {

						return Math.round((elapsed/3600)*10)/10+' h';

					}

					if(elapsed >= 86400) {

						return Math.round((elapsed/86400)*10)/10+' d';

					}

				},

				report:()=>{

					let bc = {
						report:scope.comment.stub,
						user:socket.user.data(),
					};
					socket.emit('user-comment-action',bc);
					scope.ctrl.show.report = false;

				},

				remove:()=>{

					let bc = {
						remove:scope.comment.stub,
						user:socket.user.data(),
					};
					socket.emit('user-comment-action',bc);

				},

				ban:()=>{

					let bc = {
						ban:scope.comment.user_name,
						user:socket.user.data(),
					};
					socket.emit('user-comment-action',bc);

				},

				delete:()=>{

					let bc = {
						delete:scope.comment.stub,
						user:socket.user.data(),
					};
					socket.emit('user-comment-action',bc);
					scope.ctrl.show.delete = false;

				},

				save:()=>{

					let bc = {
						comment:scope.comment.stub,
						user:socket.user.data(),
						save:0
					};

					if(scope.comment.isSaved === 0){
						bc.save = 1;
					}
					socket.emit('user-save',bc);

				}

			};

			if(scope.children){

				scope.$watch('children',()=>{

					scope.ctrl.comments = [];
					scope.ctrl.children = [];

					if (scope.children.length > 0) {
						
						for(let i=0,c=scope.children.length;i<c;i++){

							if (scope.children[i].parent === scope.comment.stub) {

								let checkIdxComment = scope.ctrl.comments.findIndex(x=>x.stub === scope.children[i].stub);
								if(checkIdxComment<0){

									scope.ctrl.comments.push(scope.children[i]);

								} else {

									scope.ctrl.comments[checkIdxComment] = scope.children[i];

								}

							} else {

								if(scope.children[i].stub !== scope.comment.stub){

									let checkIdxChildren = scope.ctrl.children.findIndex(x=>x.stub === scope.children[i].stub);
									if(checkIdxChildren<0){

										scope.ctrl.children.push(scope.children[i]);

									} else {

										scope.ctrl.children[checkIdxChildren] = scope.children[i];
										
									}

								}

							}

						}
					
					}

				},true);

			}

		}

	};

});