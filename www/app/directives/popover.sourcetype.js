'use strict';

let angular = require('angular');
let app = angular.module('nspider');

// jQuery
// let $ = require('jquery');

app.directive('popoverSourcetype',function($compile,$window,$document,$rootScope,ls) {

	return {

		restrict: 'E',
		scope:{
			type:'=',
		},
		templateUrl:'views/popovers/sourcetype.html',
		link: function (scope, el) {

			// Enable popover
			$(function () {

				$(el).popover({
					trigger: 'click',
					html: true,
					content:$compile("<div><div ng-include=\"'views/popovers/sourcetype.menu.html'\"></div></div>")(scope),
					placement: 'bottom'
				});
			
				$(document).on('click', function(){
					
					$(el).popover('hide');

				});

				$(el).click(function(){
					return false;
				});

			});

			scope.vm = {

				sourceTypeUpdate:(newType)=>{

					let pushType = newType;
					if(pushType === 'false'){
						pushType = false;
					}

					$rootScope.current.news.params.sourceType = pushType;
					
					let p = ls.get('params');
					if(p && p.params) {

						let pIdx =p.params.findIndex(x=>x.name === 'sourceType');
						if(pIdx > -1){
							p.params[pIdx] = {name:'sourceType',val:pushType};
						} else {
							p.params.push({name:'sourceType',val:pushType});
						}
						ls.set('params',p.params);

					} else {

						ls.set('params',[{name:'sourceType',val:pushType}]);
				
					}

					$rootScope.current.news.updates.reload = true;					

				},
				
			}

		}

	};

});