'use strict';

var angular = require('angular');
var app = angular.module('nspider');

app.directive('commentbox',function($rootScope,socket) {

    return {

        restrict: 'AE',
        scope: {
        	item:'=',
        	comment:'=',
            content:'=',
		},
        templateUrl:'views/directives/comment.box.html',

        link: function (scope) {

            scope.ctrl = {
                
                input:{
                    txt:scope.content,
                },
                formatting:false,

            };

            scope.vm = {

                submit:()=>{

                    let send = {
                        user:socket.user.data(),
                        txt:scope.ctrl.input.txt,
                    };
                    
                    if(scope.item){

                        send.item = scope.item;

                    }

                    if(scope.comment){

                        if(scope.content){

                            send.edit = scope.comment;

                        } else {

                            send.comment = scope.comment;    

                        }
                        
                    }

                    socket.emit('user-comment',send);
                    scope.ctrl.input.txt = scope.content;

                },
                clear:()=>{

                    scope.ctrl.input.txt = scope.content;

                },
                replyTo:()=>{

                    return scope.replyTo;

                },

            };

        }

    };

});