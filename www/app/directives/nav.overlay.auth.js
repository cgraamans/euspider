'use strict';

var angular = require('angular');
var app = angular.module('nspider');

app.directive('navOverlayAuth',function($rootScope,$location,ls,socket) {

	return {

		restrict: 'AE',
		scope: {},
        templateUrl:'views/directives/nav.overlay.auth.html',

		link: function (scope) {
			
			scope.ctrl = {

				msg:false,
				overlay:{
					init:false,
				},

			};
			scope.vm = {

				close:()=>{

					$rootScope.current.overlay.auth = false;

				}

			};

			$rootScope.$watch('current.overlay.auth',()=>{

				scope.ctrl.overlay.init = $rootScope.current.overlay.auth;

			});

			socket.s.forward('auth-register',scope);
			socket.s.forward('auth',scope);

			scope.$on('socket:auth',(evt,_d)=>{

				socket.decrypt(_d)
					.then(auth=>{

						if(auth.ok === true){

							$rootScope.current.overlay.auth = false;

						} else {

							if(auth.m){

								scope.ctrl.msg = auth.m;
								scope.$apply();

							}

						}

					});

			});

			scope.$on('socket:auth-register',(evt,_d)=>{

				socket.decrypt(_d)
					.then(auth=>{

						if(auth.ok === true){

							$rootScope.current.overlay.auth = false;
							if(auth.user){
								ls.set('auth',auth.user);
							}

						} else {

							if(auth.m){

								scope.ctrl.msg = auth.m;
								scope.$apply();

							}

						}

					});

			});

		}

	};

});
