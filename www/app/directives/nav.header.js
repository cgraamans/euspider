'use strict';

var angular = require('angular');
var app = angular.module('nspider');

app.directive('navHeader',function($rootScope,socket,$window,$timeout,ls,$location) {

	return {

		restrict: 'AE',
		scope: {
			user:'='
		},
        templateUrl:'views/directives/nav.header.html',

		link: function (scope) {

			scope.ctrl = {

				show:{

					locales:false,
					user:false,

				},  

			};

			scope.vm = {

				uri:(p)=>{
					
					$window.location.href = p;	

				},

				isLoggedIn:()=>{

					return socket.user.isLoggedIn();

				},

				logout:()=>{

					ls.remove('auth');
					scope.vm.uri('/');

				},

				themeSwitch:()=>{

					$rootScope.$emit('theme-switch',true);

				},

				isDark:()=>{
					return $rootScope.current.navs.dark;
				},

				// To be used
				getUserName:()=>{

					let rtn = false,
						data = socket.user.data();
						
					if(data){

						rtn = data.name;

					}
					
					return rtn;

				},


			};

			$rootScope.$watch('current.navs.locales',()=>{

				scope.ctrl.show.locales = $rootScope.current.navs.locales;

			});


			$rootScope.$watch('current.navs.user',()=>{

				scope.ctrl.show.user = $rootScope.current.navs.user;

			});

		}

	};

});
