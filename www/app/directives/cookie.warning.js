'use strict';

var angular = require('angular');
var app = angular.module('nspider');

app.directive('cookieWarning',function(ls,$timeout) {

    return {

        restrict: 'AE',
        scope: {},
        templateUrl:'views/directives/cookie.warning.html',

        link: function (scope) {

            scope.show = false;
            var isCookied = ls.get('cookieWarning');
            
            var startCookieRun = function() {
                $timeout(function(){
                    scope.show=true;
                },3000);
            };

            if(!isCookied) {
            
                startCookieRun();
            
            } else {

                if(!isCookied.cookieWarning) {
            
                    startCookieRun();
            
                }
            
            }

            scope.ok = function() {

                ls.set('cookieWarning',{val:true});
                scope.show=false;

            };

        }

    };

});