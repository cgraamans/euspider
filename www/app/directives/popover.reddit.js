'use strict';

let angular = require('angular');
let app = angular.module('nspider');

// jQuery
// let $ = require('jquery');

app.directive('popoverReddit',function($compile,$window) {

	return {

		restrict: 'AE',
		scope:{
			itemTitle:'=',
			itemLink:'=',
		},
		templateUrl:'views/popovers/reddit.html',
		link: function (scope, el) {

			scope.vm = {
				
				submit:()=>{

					let goTo = '';
					if(scope.ctrl.subreddit) {

						if(scope.ctrl.subreddit.length>0){

							if(scope.ctrl.subreddit.startsWith('/r/')){

								goTo = scope.ctrl.subreddit;

							} else {

								goTo = '/r/'+scope.ctrl.subreddit;

							}
							$window.open('https://www.reddit.com'+goTo+'/submit?url='+scope.itemLink+'&title='+scope.itemTitle,'_blank');

						}

					}
					
				},

			};

			scope.ctrl = {

				subreddit:'/r/europeanunion',

			};

			$(el).popover({
				trigger: 'click',
				html: true,
				content: $compile(
					[
						'<span class="share-reddit">',
						'<form>',
						'<input type="text" ng-model="ctrl.subreddit" />',
						'<input type="submit" style="float:right" ng-click="vm.submit();" value="&#xf281;" class="fa"/>',
						'</form>',
						'</span>'
					].join('\n')
				)(scope),
				placement: 'bottom'
			});

		}

	};

});