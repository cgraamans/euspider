'use strict';

var angular = require('angular');
var app = angular.module('nspider');

app.directive('navHeaderUser',function($rootScope,socket,$window,$timeout,ls,$location,$compile) {

	return {

		restrict: 'AE',
		scope: {
			user:'='
		},
        templateUrl:'views/directives/nav.header.user.html',

		link: function (scope) {

			// Enable popover
			$(function () {

				$('[data-toggle="user-popover"]').popover({
			
					placement:'bottom',
					html:true,
					title:'',
					content:$compile("<div><div ng-include=\"'views/popovers/auth.user.html'\"></div></div>")(scope),

				});
			
				$('[data-toggle="auth-popover"]').popover({
					
					title:'',
					placement:'bottom',
					html:true,
					content:$compile("<div><div ng-include=\"'views/popovers/auth.html'\"></div></div>")(scope),

				});

				$(document).on('click', function(){
					
					$('#auth-popover').popover('hide');
					$('#user-popover').popover('hide');

				});

				$('#auth-popover').click(function(){
					return false;
				});

				$('#user-popover').click(function(){
					return false;
				});

			});

			scope.ctrl = {

				show:{

					locales:false,
					user:false,

				}

			};

			scope.vm = {

				uri:(p)=>{

					for(let key in $rootScope.current.navs){

						$rootScope.current.navs[key] = false;
					
					}
					if($location.path() === p) {
					
						$window.location.href = p;	
					
					} else {
					
						$location.path(p);
					
					}

				},

				isLoggedIn:()=>{

					return socket.user.isLoggedIn();

				},

				overlay:(type,val)=>{
					
					if(!type){type = 'auth';}
					if(!val){val = 'login';}
					
					if($rootScope.current.overlay[type] === false){

						$rootScope.current.overlay[type] = val;
						$('#auth-popover').popover('hide');
						$('#user-popover').popover('hide');					
					
					} else {
					
						$rootScope.current.overlay[type] = false;
					
					}

				},

				logout:()=>{

					ls.destroy();
					socket.emit('disconnect',{ok:true});
					
					$('#auth-popover').popover('hide');
					$('#user-popover').popover('hide');

				},

				whois:()=>{

					let rtn = false,
						data = socket.user.data();
							
					if(data){

						rtn = data.name;

					}
					
					return rtn;

				}

			};

		}

	};

});
