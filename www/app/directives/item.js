'use strict';

var angular = require('angular');
var app = angular.module('nspider');

app.directive('newsItem',function(socket,$rootScope) {

	return {

		restrict: 'AE',
		scope: {
			item:'=',
			logos:'=',
		},
		templateUrl:'views/directives/item.html',

		link: function (scope) {

			scope.ctrl = {};

			scope.vm = {

				isLoggedIn:()=>{

					return socket.user.isLoggedIn();

				},
				
				date:()=>{

					let now = Math.round((new Date()).getTime()/1000);
					let elapsed = now - scope.item._dt;

					if(elapsed < 0){

						return '0 s';
					
					}

					if(elapsed < 60) {

						return elapsed+' s';

					}
					if(elapsed < 3600) {

						return Math.round((elapsed/60)*10)/10+' m';

					}

					if(elapsed < 86400) {

						return Math.round((elapsed/3600)*10)/10+' h';

					}

					if(elapsed >= 86400) {

						return Math.round((elapsed/86400)*10)/10+' d';

					}

				},

				save:()=>{

					if((scope.item) && (socket.user.isLoggedIn())) {

						let save = {

							user:socket.user.data(),
							item:scope.item.stub,
							save:1,

						};
						if(scope.item.saved > 0){
							save.save = 0;
						}

						socket.emit('user-save',save);

					}

				},

				setSource:()=>{

					$rootScope.current.news.params.source = scope.item.source_stub;
					$rootScope.current.news.updates.reload = true;

				},

				image:()=>{

					let rtn = '';
					if(scope.item.img){

						rtn = scope.item.img;

					} else {

						rtn = scope.logos + scope.item.source_logo;
						if(scope.item.source_logo === null){

							rtn = '/img/eu.jpg';

						}							

					}
					return rtn;

				},

				isAuthLvl:(lvl)=>{

					let rtn = false;
					if(!lvl){
						lvl = 1;
					}
					if(scope.vm.isLoggedIn() === true){

						let uD = socket.user.data();

						// console.log(uD);
						if(uD){

							if(uD.auth){

								if(uD.auth>=lvl){

									rtn = true;

								}

							}

						}

					}
					return rtn;

				},

				encodeURI:(str)=>{
					return encodeURI(str);
				}

			};

			scope.$watch('item',()=>{

				if(scope.item){

					scope.item._title = twemoji.parse(scope.item.title,(icon)=>{
						return 'https://twemoji.maxcdn.com/2/svg/' + icon + '.svg';
					});					

				}

			},true);

		}

	};

});