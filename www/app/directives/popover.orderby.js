'use strict';

let angular = require('angular');
let app = angular.module('nspider');

// jQuery
// let $ = require('jquery');

app.directive('popoverOrderby',function($compile,$window,$document,$rootScope,ls) {

	return {

		restrict: 'E',
		scope:{
			order:'=',
			hideTitle:'@',
		},
		templateUrl:'views/popovers/orderby.html',
		link: function (scope, el) {

			// Enable popover
			$(function () {

				$(el).popover({
					trigger: 'click', 
					html: true,
					content:$compile("<div><div ng-include=\"'views/popovers/orderby.menu.html'\"></div></div>")(scope),
					placement: 'bottom'
				});
			
				$(document).on('click', function(){
					
					$(el).popover('hide');

				});

				$(el).click(function(){
					return false;
				});

			});

			scope.vm = {

				orderby:(newOrder)=>{

					$rootScope.current.news.params.orderBy = newOrder;

					let p = ls.get('params');
					if(p && p.params) {

						let pIdx =p.params.findIndex(x=>x.name === 'orderBy');
						if(pIdx > -1){
							p.params[pIdx] = {name:'orderBy',val:newOrder};
						} else {
							p.params.push({name:'orderBy',val:newOrder});
						}
						ls.set('params',p.params);

					} else {

						ls.set('params',[{name:'orderBy',val:newOrder}]);
				
					}

					$rootScope.current.news.updates.reload = true;

				}

			}

		}

	};

});