'use strict';

var angular = require('angular');
var app = angular.module('nspider');

app.directive('source',function(socket,$rootScope) {

	return {

		restrict: 'AE',
		scope: {
			data:'=',
			logos:'=',
		},
		templateUrl:'views/directives/source.html',

		link: function (scope) {

			scope.ctrl = {};
			socket.s.forward('user-save',scope);

			scope.vm = {

				isLoggedIn:()=>{

					return socket.user.isLoggedIn();

				},
				
				date:()=>{

					let now = Math.round((new Date()).getTime()/1000);
					let elapsed = now - scope.item._dt;

					if(elapsed < 0){

						return '0 s';
					
					}

					if(elapsed < 60) {

						return elapsed+' s';

					}
					if(elapsed < 3600) {

						return Math.round((elapsed/60)*10)/10+' m';

					}

					if(elapsed < 86400) {

						return Math.round((elapsed/3600)*10)/10+' h';

					}

					if(elapsed >= 86400) {

						return Math.round((elapsed/86400)*10)/10+' d';

					}

				},

				sub:()=>{
					
					let emit = {
						_user:socket.user.data(),
						save:1,
						source:scope.data.stub,
					};
					socket.emit('user-save',emit);

				},

				unsub:()=>{

					let emit = {
						_user:socket.user.data(),
						save:0,
						source:scope.data.stub,
					};
					socket.emit('user-save',emit);

				},

				close:()=>{

					$rootScope.current.news.params.source = false;
					$rootScope.current.news.updates.reload = true;

				}

			};

		}

	};

});