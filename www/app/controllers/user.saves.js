'use strict';

module.exports = function($scope,socket,$routeParams,$rootScope) {

	//
	// Local function declaration
	//

	let fn={

		assemble:(param)=>{

			let rtn = false;
			if(param.indexOf(':')){

				let paramArr = param.split(':');
				if(paramArr.length === 2) {

					rtn = paramArr;

				}

			}
			return rtn;

		},
		
		prepParam:()=>{

			let rtn = {};
			if($routeParams.params) {

				if($routeParams.params.indexOf(',') > -1){

					let splitArr = $routeParams.params.split(',');
					if(splitArr.length > 0) {
						splitArr.forEach(param=>{

							let fnas = fn.assemble(param);
							if(fnas){

								rtn[fnas[0]] = fnas[1];

							}

						});
					}

				} else {

					let fnas = fn.assemble($routeParams.params);
					if(fnas){

						rtn[fnas[0]] = fnas[1];

					}

				}

			}

			if(socket.user.isLoggedIn()){

				rtn.user = socket.user.data();
			
			}

			return rtn;

		},

	};

	//
	// Variable Definition
	//

	$scope.ctrl = {

		nav:{

			bottom:{
				expand:false,
			},
			tab:false,
			category:'likes',

		},
		comments:[],
		children:[],
		items:[],
		sort:['-votes','-_dt'],
	
	};

	if($routeParams.type){
		$scope.ctrl.nav.tab = $routeParams.type;
	} else {
		$scope.ctrl.nav.tab = 'items';
	}

	$scope.vm = {

		userName:()=>{
			return socket.user.data().name;
		},

		orderBy:(cat)=>{

			switch(cat){

				case 'new':
					$scope.ctrl.sort = ['-_dt'];
					break;
				case 'likes':
					$scope.ctrl.sort = ['-votes','-_dt'];
					break;
				case 'dislikes':
					$scope.ctrl.sort = ['votes','-_dt'];
					break;
				case 'old':
					$scope.ctrl.sort = ['_dt'];
					break;
				default:
					$scope.ctrl.sort = ['-votes','-_dt'];
					break;

			}

		},


	};

	// 
	// Condition Initial Params
	//

	let params = fn.prepParam();

	socket.s.forward('comments',$scope);
	socket.s.forward('items',$scope);

	$scope.$on('socket:items',(evt,_d)=>{

		socket.decrypt(_d)
			.then(items=>{
				if(items.ok === true){

					if(items.items.length>0){

						if($scope.ctrl.items.length===0) {

							$scope.ctrl.items = items.items;
							$scope.$apply();							

						} else {

							for(let i=0,c=items.items.length;i<c;i++){

								let v = $scope.ctrl.items.findIndex(x=>x.stub === items.items[i]);
								if(v<0) {

									$scope.ctrl.items.push(items.items[i]);

								}

							}

						}

					}

				} else {

					$rootScope.current.log.push({t:'e',e:'Error in items',d:items,dt:Math.round((new Date()).getTime()/1000)});

				}

			}).catch(e=>{

				$rootScope.current.log.push({t:'e',e:'Error in items',d:e,dt:Math.round((new Date()).getTime()/1000)});

			});
	});

	$scope.$on('socket:comments',(evt,_d)=>{

		socket.decrypt(_d)
			.then(commentObj=>{

				if(commentObj.ok === true) {

					if(commentObj.comments.length>0) {

						if($scope.ctrl.comments.length===0) {

							$scope.ctrl.comments = commentObj.comments;
							$scope.$apply();

						} else {

							for(let i=0,c=commentObj.comments.length;i<c;i++) {

								let v = $scope.ctrl.comments.findIndex(x=>x.stub === commentObj.comments[i]);
								if(v<0) {

									$scope.ctrl.children.push(commentObj.comments[i]);

								} else {

									$scope.ctrl.children[v] = commentObj.comments[i];

								}

							}
							$scope.$apply();

						}

					}

				} else {

					$rootScope.current.log.push({t:'e',e:'Error in comments',d:commentObj,dt:Math.round((new Date()).getTime()/1000)});

				}

			})
			.catch(e=>{

					$rootScope.current.log.push({t:'e',e:'Error in comments',d:e,dt:Math.round((new Date()).getTime()/1000)});

			});

	});

	let condComment = Object.assign({},params),
		condItems =  Object.assign({},params);
		
	condComment.by = $routeParams.user;
	condComment.saved = true;		
	socket.emit('comments',condComment);
	
	condItems.savedBy = $routeParams.user;
	socket.emit('items',condItems);

};