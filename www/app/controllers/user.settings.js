'use strict';

module.exports = function($scope,socket,$routeParams,$rootScope,ls) {

	//
	// Local function declaration
	//

	let fn = {

		assemble:(param)=>{

			let rtn = false;
			if(param.indexOf(':')){

				let paramArr = param.split(':');
				if(paramArr.length === 2) {

					rtn = paramArr;

				}

			}
			return rtn;

		},
		
		condition:()=>{
			
			let rtn = {
				source:$routeParams.source
			};
			if($routeParams.params) {

				if($routeParams.params.indexOf(',') > -1){

					let splitArr = $routeParams.params.split(',');
					if(splitArr.length > 0) {
						splitArr.forEach(param=>{

							let p = fn.assemble(param);
							if(p){
								rtn[p[0]] = p[1];
							}

						});
					}

				} else {

					let p = fn.assemble($routeParams.params);
					if(p){
						rtn[p[0]] = p[1];
					}

				}

			}

			if(socket.user.isLoggedIn()){

				rtn.user = socket.user.data();
			
			}

			return rtn;

		},

	};	

	//
	// Variable Definition
	//

	$scope.ctrl = {

		secret:false,
		api:{
			id:false,
			token:false,
		},
		pass:{
			init:'',
			rpt:'',
			show:false,
			err:[],
		},
		connection:false,
		email:{
			show:false,
			init:'',
		},
		log:{
			show:false,
			data:[],
		},

	};

	$scope.vm = {

		isLoggedIn:()=>{

			return socket.user.isLoggedIn();

		},

		secret:()=>{

			return socket.secret;
		
		},
		api:()=>{

			return socket.user.data();

		},

		retrieve:{

			secret:()=>{

				socket.emit('user-settings',{user:socket.user.data(),action:'set',type:'secret'});

			},

			token:()=>{

				socket.emit('user-settings',{user:socket.user.data(),action:'set',type:'token'});

			},

		},

		proc:{

			pass:()=>{

				if($scope.ctrl.pass.init === $scope.ctrl.pass.rpt){

					socket.emit('user-settings',{user:socket.user.data(),action:'set',type:'password',password:$scope.ctrl.pass.init});
				
				} else {
				
					$scope.ctrl.pass.err.push('Passwords must match');
				
				}

			},
			email:()=>{
				
				if($scope.ctrl.email.init.length>0){
					socket.emit('user-settings',{user:socket.user.data(),action:'set',type:'email',email:$scope.ctrl.email.init});
				}
				

			}

		}


	};

	socket.s.forward('user-settings',$scope);
	$scope.$on('socket:user-settings',(evt,_d)=>{

		socket.decrypt(_d)
			.then(settingsData=>{

				if(settingsData.ok === true) {

					if((settingsData.type === 'token') && (settingsData.token)) {

						let sud = socket.user.data();
						if(sud){

							sud.token = settingsData.token;
							ls.set('auth',sud);

						}

					}
					if((settingsData.type === 'email') && (settingsData.email)) {
						$scope.ctrl.email.init = settingsData.email;
					}
					$scope.$apply();

				} else {

					$rootScope.current.log.push({t:'e',e:'Error in user-settings',d:settingsData,dt:Math.round((new Date()).getTime()/1000)});

				}
			
			})
			.catch(e=>{

				$rootScope.current.log.push({t:'e',e:'Error in user-settings',d:e,dt:Math.round((new Date()).getTime()/1000)});

			});
	});

	$scope.$watch('vm.secret()',()=>{

		$scope.ctrl.secret = $scope.vm.secret();

	});

	$scope.$watch('vm.api()',(n)=>{

		if(n.token){

			if($scope.ctrl.api.token !== n.token){
			
				$scope.ctrl.api = $scope.vm.api();

			}

		}

	},true);

	$rootScope.$watch('current.log',(n,o)=>{

		if(n!==o){
			$scope.ctrl.log.data = $rootScope.current.log;	
		}

	},true);

	socket.emit('user-settings',{user:socket.user.data(),action:'get',type:'email'});

};