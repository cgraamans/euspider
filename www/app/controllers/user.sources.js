'use strict';

module.exports = function($scope,socket,$routeParams,$rootScope,$interval) {

	//
	// Local function declaration
	//

	let fn={

		assemble:(param)=>{

			let rtn = false;
			if(param.indexOf(':')){

				let paramArr = param.split(':');
				if(paramArr.length === 2) {

					rtn = paramArr;

				}

			}
			return rtn;

		},
		
		prepParam:()=>{

			let rtn = {
				sourced:$routeParams.user
			};
			if($routeParams.params) {

				if($routeParams.params.indexOf(',') > -1){

					let splitArr = $routeParams.params.split(',');
					if(splitArr.length > 0) {
						splitArr.forEach(param=>{

							let fnas = fn.assemble(param);
							if(fnas){

								rtn[fnas[0]] = fnas[1];

							}

						});
					}

				} else {

					let fnas = fn.assemble($routeParams.params);
					if(fnas){

						rtn[fnas[0]] = fnas[1];

					}

				}

			}

			if(socket.user.isLoggedIn()){

				rtn.user = socket.user.data();
			
			}

			return rtn;

		},

	};

	//
	// Variable Definition
	//

	let params = fn.prepParam();

	$scope.ctrl = {

		nav:{

			isLoading:true,
			bottom:{
				expand:false,
			},
			category:'new',
			items:{

				update:Math.round((new Date()).getTime()/1000),
				logos:require('../options/http').uri.images,
				up:0,
		
			},

		},
		items:[],
	
	};

	$scope.vm = {

		orderBy:()=>{

			let rtn = [];
			switch($scope.ctrl.nav.category){

				case 'new':
					rtn = ['-_dt'];
					break;
				case 'likes':
					rtn = ['-votes','-_dt'];
					break;
				case 'dislikes':
					rtn = ['votes','-_dt'];
					break;
				case 'comments':
					rtn = ['-comments','-_dt'];
					break;
				case 'old':
					rtn = ['_dt'];
					break;

			}
			return rtn;

		},

		more:()=>{
			
			if($scope.ctrl.items.length>0){

				let cond = Object.assign({}, params);

				cond.category = $scope.ctrl.nav.category;
				cond.from = $scope.ctrl.items[$scope.ctrl.items.length-1].stub;

				socket.emit('items',cond);	
			
			}

		},

		isLoggedIn:()=>{

			return socket.user.isLoggedIn();
		
		}

	};
	// 
	// Condition Initial Params
	//

	// Items
	socket.s.forward('items',$scope);
	$scope.$on('socket:items',(evt,_d)=>{

		socket.decrypt(_d)
			.then(data=>{

				if(data.ok === true){

					if(data.category === $scope.ctrl.nav.category) {

						data.items.forEach(item=>{
	
							let isItem = $scope.ctrl.items.findIndex(x=> x.stub === item.stub);
							if(isItem < 0){

								$scope.ctrl.items.push(item);
								$scope.$apply();

							}

						});

						$scope.ctrl.nav.isLoading = false;
						$scope.$apply();

					}

				} else {

					$rootScope.current.log.push({t:'e',e:'Error in items',d:data,dt:Math.round((new Date()).getTime()/1000)});

				}

			})
			.catch(e=>{

				$rootScope.current.log.push({t:'e',e:'Error in items',d:e,dt:Math.round((new Date()).getTime()/1000)});

			});

	});


	// News Update
	socket.s.forward('items-ping',$scope);	
	let intervalNewsUpdate = $interval(()=>{

		let cond = Object.assign({}, params);

		cond.category = $scope.ctrl.nav.category;		
		cond.up = $scope.ctrl.nav.items.update;
		
		if($scope.ctrl.nav.category === 'new'){

			socket.emit('items',cond);

		} else {

			socket.emit('items-ping',cond);

		}

	},10000);
	$scope.$on('socket:items-ping',(event,_d)=>{

		socket.decrypt(_d)
			.then(items=>{

				console.log(items);

			})
			.catch(e=>{

				$rootScope.current.log.push({t:'e',e:'Error in items-ping',d:e,dt:Math.round((new Date()).getTime()/1000)});
				$interval.cancel(intervalNewsUpdate);

			});

	});

	// Category Watch
	$scope.$watch('ctrl.nav.category',()=>{

		let cond = Object.assign({}, params);
		cond.category = $scope.ctrl.nav.category;

		if($scope.ctrl.nav.isLoading !== true){

			socket.emit('items',cond);
			
			$scope.ctrl.nav.isLoading = true;
			$scope.ctrl.items = [];

			if($scope.ctrl.nav.category === 'new'){

				$scope.ctrl.nav.items.update = Math.round((new Date()).getTime()/1000);	
			
			}

		}

	});

	// Initial Trigger
	let cond = Object.assign({}, params);
	cond.category = $scope.ctrl.nav.category;	
	socket.emit('items',cond);

	// Destroy
	$scope.$on('$destroy',()=>{

		$interval.cancel(intervalNewsUpdate);

	});

};