'use strict';

module.exports = function($scope,socket,$routeParams,$rootScope,$interval,ls) {

	//
	// Local function declaration
	//
	
	$scope.ctrl = {

		config:{
			
			logos:require('../options/http').uri.images,

			items:{
	
				update:Math.round((new Date()).getTime()/1000),
				up:0,

			},
			isLoading:false,

		},
		params:$rootScope.current.news.params,
		orderby:$rootScope.current.news.params.category,

		items:[],
		source:false,
		hasNew:false,

	};

	let params = {

		get:()=>{

			let rtn = Object.assign({}, $rootScope.current.news.params);
			if(socket.user.isLoggedIn()){

				rtn._user = socket.user.data();
			
			}
			return rtn;

		},

		_proc:(param)=>{

			let rtn = false;
			if(param.indexOf(':')){

				let paramArr = param.split(':');
				if(paramArr.length === 2) {

					rtn = paramArr;

				}

			}
			return rtn;

		},
			
		route:()=>{

			if($routeParams.params) {

				if($routeParams.params.indexOf(',') > -1){

					let splitArr = $routeParams.params.split(',');
					if(splitArr.length > 0) {
						splitArr.forEach(param=>{

							let paramArr = params._proc(param);
							if(paramArr!==false){

								$rootScope.current.news.params[paramArr[0]] = paramArr[1];

							}

						});

					}

				} else {

					let paramArr = params._proc($routeParams.params);
					if(paramArr!==false){

						$rootScope.current.news.params[paramArr[0]] = paramArr[1];

					}

				}

			} else {

				let p = ls.get('params');
				if(p && p.params){
					p.params.forEach(pr=>{
						$rootScope.current.news.params[pr.name] = pr.val;	
					});
				}

			}


		},

		execute:param=>{

			$scope.ctrl.config.isLoading = true;
			$scope.ctrl.items = [];

			if(param.source){

				let sourceRequest = {
						source:param.source
				};
				if(socket.user.isLoggedIn()){
					sourceRequest._user = socket.user.data();
				}

				socket.emit('source',sourceRequest);
			}

			socket.emit('items',param);

		}

	};
	params.route();

	$scope.vm = {

		orderBy:()=>{

			let rtn = [];
			switch($rootScope.current.news.params.orderBy){

				case 'dtUp':
					rtn = ['-_dt','-pubDate'];
					break;
				case 'votesUp':
					rtn = ['-votes','-_dt','-pubDate'];
					break;
				case 'votesDown':
					rtn = ['votes','-_dt','-pubDate'];
					break;
				case 'commentsUp':
					rtn = ['-comments','-_dt','-pubDate'];
					break;
				case 'dtDown':
					rtn = ['_dt','pubDate'];
					break;

			}
			return rtn;

		},

		more:()=>{
			
			if($scope.ctrl.items.length>0){

				let cond = params.get();
				cond.from = $scope.ctrl.items[$scope.ctrl.items.length-1].stub;

				socket.emit('items',cond);	
			
			}

		},

		isLoggedIn:()=>{

			return socket.user.isLoggedIn();
		
		},

		getParam:(paramName)=>{

			let rtn = false;
			if($rootScope.current.news.params[paramName]) {
				rtn = $rootScope.current.news.params[paramName];
			}
			return rtn;

		},

		setParam(name,val){

			let insVal = val;
			if(val === 'false'){
				insVal = false;
			}
			$rootScope.current.news.params[name] = insVal;

			let p = ls.get('params');
			if(p && p.params) {

				let pIdx =p.params.findIndex(x=>x.name === name);
				if(pIdx > -1){
					p.params[pIdx] = {name:name,val:insVal};
				} else {
					p.params.push({name:name,val:insVal});
				}
				ls.set('params',p.params);

			} else {

				ls.set('params',[{name:name,val:insVal}]);
		
			}
			
			$rootScope.current.news.updates.reload = true;

		}

	};

	//
	// Emitters
	//

	socket.s.forward('items',$scope);
	socket.s.forward('items-ping',$scope);
	socket.s.forward('user-vote',$scope);
	socket.s.forward('user-save',$scope);
	socket.s.forward('source',$scope);

	$scope.$on('socket:items',(event,_d)=>{

		socket.decrypt(_d)
			.then(data=>{

				if(data.ok === true){

					if($scope.ctrl.params.orderBy) {

						if($scope.ctrl.params.orderBy === 'dtUp') {

							$rootScope.current.news.updates.items = 0;

						}
					
					}

					data.items.forEach(item=>{

						let isItem = $scope.ctrl.items.findIndex(x=> x.stub === item.stub);
						if(isItem < 0){

							$scope.ctrl.items.push(item);
							$scope.$apply();

						}

					});

					$scope.ctrl.config.isLoading = false;
					$scope.$apply();

				} else {

					$rootScope.current.log.push({t:'e',e:'Error in items',d:data,dt:Math.round((new Date()).getTime()/1000)});

				}

			}).catch(e=>{

				$rootScope.current.log.push({t:'e',e:'Error in items',d:e,dt:Math.round((new Date()).getTime()/1000)});

			});

	});


	$scope.$on('socket:source',(evt,_d)=>{

		socket.decrypt(_d)
			.then(sourceObj=>{

				if(sourceObj.ok === true){

					$scope.ctrl.source = sourceObj.source;
					$scope.$apply();

				} else {

					$rootScope.current.log.push({t:'e',e:'Error in items-ping',d:sourceObj,dt:Math.round((new Date()).getTime()/1000)});

				}

			})
			.catch(e=>{

				$rootScope.current.log.push({t:'e',e:'Error in source',d:e,dt:Math.round((new Date()).getTime()/1000)});

			});

	});

	$scope.$on('socket:items-ping',(event,_d)=>{

		socket.decrypt(_d)
			.then(items=>{

				$scope.ctrl.hasNew = false;
				if(items.ok === true){

					if(items.count > 0) {

						$scope.ctrl.hasNew = true;
						$rootScope.current.news.updates.items = items.count;

					}

				}

			})
			.catch(e=>{

				$rootScope.current.log.push({t:'e',e:'Error in items-ping',d:e,dt:Math.round((new Date()).getTime()/1000)});
				$interval.cancel(intervalNewsUpdate);

			});

	});
	
	$scope.$on('socket:user-vote',(event,_d)=>{
		
		socket.decrypt(_d)
			.then(vote=>{

				if(vote.ok === true){

					if($scope.ctrl.items.length>0){

						let idx = $scope.ctrl.items.findIndex(x=>x.stub === vote.item);
						if(idx>-1){
							
							$scope.ctrl.items[idx].votes = vote.votes;
							$scope.ctrl.items[idx].vote = vote.vote;
							$scope.$apply();

						}

					}

				} else {

					$rootScope.current.log.push({t:'e',e:'Error in user-vote',d:vote,dt:Math.round((new Date()).getTime()/1000)});

				}
				
			}).catch(e=>{

				$rootScope.current.log.push({t:'e',e:'Error in user-vote',d:e,dt:Math.round((new Date()).getTime()/1000)});

			});

	});

	$scope.$on('socket:user-save',(event,_d)=>{

		socket.decrypt(_d)
			.then(saveObj=>{

				if(saveObj.ok === true){

					if(($scope.ctrl.items.length>0) && (saveObj.item)){

						let idx = $scope.ctrl.items.findIndex(x=>x.stub === saveObj.item);
						if(idx>-1){

							$scope.ctrl.items[idx].saved = saveObj.save;
							$scope.$apply();

						}

					}
					if(($scope.ctrl.source) && saveObj.source) {

						if(saveObj.source === $scope.ctrl.source.stub) {

							$scope.ctrl.source.isFollowed = saveObj.save;
							$scope.$apply();

						}

					}


				} else {

					$rootScope.current.log.push({t:'e',e:'Error in user-save',d:saveObj,dt:Math.round((new Date()).getTime()/1000)});

				}

			}).catch(e=>{

				$rootScope.current.log.push({t:'e',e:'Error in user-save',d:e,dt:Math.round((new Date()).getTime()/1000)});

			});

	});

	$rootScope.$watch('current.news.updates.reload',()=>{

		if($rootScope.current.news.updates.reload){
			
			let cond = params.get();
			params.execute(cond);
			$rootScope.current.news.updates.reload = false;

		}

	});

	let intervalNewsUpdate = $interval(()=>{

		let send = params.get(),
			isNew = false;

		if(send.orderBy){

			if(send.orderBy === 'dtUp'){

				socket.emit('items',send);
				$scope.ctrl.config.items.update = Math.round((new Date()).getTime()/1000);
				isNew = true;

			}

		}
		if(!isNew) {

			let newSend = {
				up:$scope.ctrl.config.items.update,
				category:'new'
			};
			socket.emit('items-ping',newSend);

		}

	},10000);

	$rootScope.current.news.updates.reload = true;

	$scope.$on('$destroy',()=>{

		$interval.cancel(intervalNewsUpdate);

	});

};