'use strict';

module.exports = function($scope,socket,$routeParams,$rootScope) {

	//
	// Local function declaration
	//

	let fn={

		assemble:(param)=>{

			let rtn = false;
			if(param.indexOf(':')){

				let paramArr = param.split(':');
				if(paramArr.length === 2) {

					rtn = paramArr;

				}

			}
			return rtn;

		},
		
		prepParam:()=>{

			let rtn = {
				by:$routeParams.user
			};
			if($routeParams.params) {

				if($routeParams.params.indexOf(',') > -1){

					let splitArr = $routeParams.params.split(',');
					if(splitArr.length > 0) {
						splitArr.forEach(param=>{

							let fnas = fn.assemble(param);
							if(fnas){

								rtn[fnas[0]] = fnas[1];

							}

						});
					}

				} else {

					let fnas = fn.assemble($routeParams.params);
					if(fnas){

						rtn[fnas[0]] = fnas[1];

					}

				}

			}

			if(socket.user.isLoggedIn()){

				rtn.user = socket.user.data();
			
			}

			return rtn;

		},

	};

	//
	// Variable Definition
	//

	$scope.ctrl = {

		nav:{

			bottom:{
				expand:false,
			},

		},
		comments:[],
		children:[],
	
	};

	$scope.vm = {

	};

	// 
	// Condition Initial Params
	//

	let params = fn.prepParam();

	socket.s.forward('user-vote',$scope);
	socket.s.forward('user-save',$scope);

	$scope.$on('socket:user-vote',(evt,_d)=>{

		socket.decrypt(_d)
			.then(userVote=>{

				if(userVote.ok === true){

					if(userVote.comment){

						let cIdx = $scope.ctrl.comments.findIndex(x=>x.stub===userVote.comment);						
						if(cIdx>-1){

							$scope.ctrl.comments[cIdx].votes = userVote.votes;
							$scope.ctrl.comments[cIdx].isVotedOn = userVote.vote;

						}

					}
					$scope.$apply();

				} else {

					$rootScope.current.log.push({t:'e',e:'Error in user-vote',d:userVote,dt:Math.round((new Date()).getTime()/1000)});

				}

			}).catch(e=>{

				$rootScope.current.log.push({t:'e',e:'Error in user-vote',d:e,dt:Math.round((new Date()).getTime()/1000)});

			});

	});

	$scope.$on('socket:user-save',(evt,_d)=>{

		socket.decrypt(_d)
			.then(userSave=>{

				if(userSave.ok === true){

					if((userSave.item) && ($scope.ctrl.item)){

						if(userSave.item === $scope.ctrl.item.stub) {

							$scope.ctrl.item.saved = userSave.save;
							$scope.$apply();

						}

					}

					if(userSave.comment){

						let cIdx = $scope.ctrl.comments.findIndex(x=>x.stub === userSave.comment);
						if(cIdx>-1){

							$scope.ctrl.comments[cIdx].isSaved = userSave.save;

						}
						$scope.$apply();

					}

				} else {

					$rootScope.current.log.push({t:'e',e:'Error in user-save',d:userSave,dt:Math.round((new Date()).getTime()/1000)});

				}

			}).catch(e=>{

				$rootScope.current.log.push({t:'e',e:'Error in user-save',d:e,dt:Math.round((new Date()).getTime()/1000)});

			});

	});

	socket.s.forward('comments',$scope);

	$scope.$on('socket:comments',(evt,_d)=>{

		socket.decrypt(_d)
			.then(commentObj=>{

				if(commentObj.ok === true) {

					if(commentObj.comments.length>0) {

						if($scope.ctrl.comments.length===0) {

							for(let i=0,c=commentObj.comments.length;i<c;i++) {

								let v = $scope.ctrl.comments.findIndex(x=>x.stub === commentObj.comments[i].stub);
								if(v<0) {

									$scope.ctrl.comments.push(commentObj.comments[i]);

								} else {

									$scope.ctrl.comments[v] = commentObj.comments[i];

								}

							}
							$scope.$apply();

						}

					}

				} else {

					$rootScope.current.log.push({t:'e',e:'Error in comments',d:commentObj,dt:Math.round((new Date()).getTime()/1000)});


				}

			})
			.catch(e=>{

				$rootScope.current.log.push({t:'e',e:'Error in comments',d:e,dt:Math.round((new Date()).getTime()/1000)});

			});

	});

	socket.emit('comments',params);

};