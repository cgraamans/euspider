'use strict';

module.exports = function($scope,$rootScope,socket) {

	$rootScope.current.page = '';

	$scope.ctrl = {
		l_persistent:true,
		r_persistent:true,
	};
	$scope.vm = {
		
		login:()=>{

			let bc = {
				name:$scope.ctrl.l_name,
				password:$scope.ctrl.l_pwd,
			};
			if($scope.ctrl.l_persistent){
				bc.persistent = $scope.ctrl.l_persistent;
			}
			socket.emit('auth',bc);
		
		},
		register:()=>{

			let bc = {
				name:$scope.ctrl.r_name,
				password:$scope.ctrl.r_pwd,
			};
			if($scope.ctrl.r_persistent){
				bc.persistent = $scope.ctrl.r_persistent;
			}
			socket.emit('auth-register',bc);

		},
		
	};

};