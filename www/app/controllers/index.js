'use strict';

var app = require('angular').module('nspider');

app.controller('AuthController',require('./auth'));

app.controller('NewsController',require('./news'));
app.controller('ItemController',require('./item'));

app.controller('UserCommentsController',require('./user.comments'));
app.controller('UserSavesController',require('./user.saves'));
app.controller('UserSourcesController',require('./user.sources'));
app.controller('UserSettingsController',require('./user.settings'));

