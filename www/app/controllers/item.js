'use strict';
var angular = require('angular');

module.exports = function($scope,$routeParams,socket,$rootScope,$document) {

	let fn={

		params:{

			assemble:(param)=>{

				let rtn = false;
				if(param.indexOf(':')){

					let paramArr = param.split(':');
					if(paramArr.length === 2) {

						rtn = paramArr;

					}

				}
				return rtn;

			},
			
			prepParam:()=>{

				let rtn = {};
				if($routeParams.params) {

					if($routeParams.params.indexOf(',') > -1){

						let splitArr = $routeParams.params.split(',');
						if(splitArr.length > 0) {
							splitArr.forEach(param=>{

								let fnas = fn.params.assemble(param);
								if(fnas){

									rtn[fnas[0]] = fnas[1];

								}

							});
						}

					} else {

						let fnas = fn.params.assemble($routeParams.params);
						if(fnas){

							rtn[fnas[0]] = fnas[1];

						}

					}

				}

				if(socket.user.isLoggedIn()){

					rtn.user = socket.user.data();
				
				}

				return rtn;

			},

			prepParamItem:()=>{


				let rtn = {
					item:$routeParams.item
				};
				if(socket.user.isLoggedIn()){

					rtn.user = socket.user.data();
				
				}
				return rtn;

			},


		},

		comments:{

			proc:(commentObj)=>{

				if(commentObj.row.parent === null){

					let chIdx = $scope.ctrl.comments.findIndex(x=>x.stub===commentObj.row.stub);						
					if(chIdx<0){

						$scope.ctrl.comments.push(commentObj.row);	

					} else {

						$scope.ctrl.comments[chIdx] = commentObj.row;
					
					}

				} else {

					let chIdx = $scope.ctrl.children.findIndex(x=>x.stub===commentObj.row.stub);						
					if(chIdx<0){

						$scope.ctrl.children.push(commentObj.row);	

					} else {

						$scope.ctrl.children[chIdx] = commentObj.row;
					
					}

				}
				$scope.$apply();

			}

		}

	};

	$scope.ctrl = {

		nav:{

			bottom:{
				expand:false,
			},
			items:{
				logos:require('../options/http').uri.images,
			},
			reply:false,
			category:'likes',

		},
		sort:['-votes','-dt'],

		item:false,
		comments:[],
		children:[],

	};

	$scope.vm = {

		isLoggedIn:()=>{

			return socket.user.isLoggedIn();

		},

		login:()=>{

			$rootScope.current.overlay.auth = true;
			
		},

		orderBy:(cat)=>{

			switch(cat){

				case 'new':
					$scope.ctrl.sort = ['-_dt'];
					break;
				case 'likes':
					$scope.ctrl.sort = ['-votes','-_dt'];
					break;
				case 'dislikes':
					$scope.ctrl.sort = ['votes','-_dt'];
					break;
				case 'old':
					$scope.ctrl.sort = ['_dt'];
					break;
				default:
					$scope.ctrl.sort = ['-votes','-_dt'];
					break;

			}
			
		},

		
	};

	let params = fn.params.prepParam();
	
	socket.s.forward('items',$scope);
	socket.s.forward('comments',$scope);
	socket.s.forward('user-comment',$scope);
	socket.s.forward('user-comment-action',$scope);
	socket.s.forward('user-vote',$scope);
	socket.s.forward('user-save',$scope);

	$scope.$on('socket:user-vote',(evt,_d)=>{

		socket.decrypt(_d)
			.then(userVote=>{

				if(userVote.ok === true){

					if((userVote.item) && ($scope.ctrl.item)){

						if(userVote.item === $scope.ctrl.item.stub) {

							$scope.ctrl.item.vote = userVote.vote;
							$scope.ctrl.item.votes = userVote.votes;

						}

					}

					if(userVote.comment){

						let cIdx = $scope.ctrl.comments.findIndex(x=>x.stub===userVote.comment);						
						if(cIdx>-1){

							$scope.ctrl.comments[cIdx].votes = userVote.votes;
							$scope.ctrl.comments[cIdx].isVotedOn = userVote.vote;

						}

						let chIdx = $scope.ctrl.children.findIndex(x=>x.stub===userVote.comment);						
						if(chIdx>-1){

							$scope.ctrl.children[chIdx].votes = userVote.votes;
							$scope.ctrl.children[chIdx].isVotedOn = userVote.vote;

						}

					}
					$scope.$apply();

				}

			}).catch(e=>{

				$rootScope.current.log.push({t:'e',e:'Error in user-vote',d:e,dt:Math.round((new Date()).getTime()/1000)});

			});

	});

	$scope.$on('socket:user-save',(evt,_d)=>{

		socket.decrypt(_d)
			.then(userSave=>{

				if(userSave.ok === true){

					if((userSave.item) && ($scope.ctrl.item)){

						if(userSave.item === $scope.ctrl.item.stub) {

							$scope.ctrl.item.saved = userSave.save;
							$scope.$apply();

						}

					}

					if(userSave.comment){

						let cIdx = $scope.ctrl.comments.findIndex(x=>x.stub === userSave.comment);
						if(cIdx>-1){

							$scope.ctrl.comments[cIdx].isSaved = userSave.save;

						}
						$scope.$apply();

					}

				} else {

					$rootScope.current.log.push({t:'e',e:'Error in user-save',d:userSave,dt:Math.round((new Date()).getTime()/1000)});

				}

			}).catch(e=>{

				$rootScope.current.log.push({t:'e',e:'Error in user-save',d:e,dt:Math.round((new Date()).getTime()/1000)});

			});

	});

	$scope.$on('socket:items',(evt,_d)=>{

		socket.decrypt(_d)
			.then(items=>{

				if(items.ok === true){

					$scope.ctrl.item = items.items[0];
					$scope.$apply();

					let cond = Object.assign({},params);
					if(!cond.comment){
	
						if($routeParams.item){

							cond.item = $routeParams.item;
						
						}
	
					}
					socket.emit('comments',cond);

				} else {

					$rootScope.current.log.push({t:'e',e:'Error in items',d:items,dt:Math.round((new Date()).getTime()/1000)});

				}

			})
			.catch(e=>{

				$rootScope.current.log.push({t:'e',e:'Error in items',d:e,dt:Math.round((new Date()).getTime()/1000)});

			});

	});

	$scope.$on('socket:comments',(evt,_d)=>{

		socket.decrypt(_d)
			.then(commentObj=>{

				if((commentObj.ok === true) && (commentObj.comments)) {

					if(commentObj.comments.length>0) {

						if($scope.ctrl.comments.length===0) {

							for(let i=0,c=commentObj.comments.length;i<c;i++) {

								if(commentObj.comments[i].parent === null){

									$scope.ctrl.comments.push(commentObj.comments[i]);

								} else {

									if(params.comment){

										if(params.comment === commentObj.comments[i].stub){

											$scope.ctrl.comments.push(commentObj.comments[i]);

										} else {

											$scope.ctrl.children.push(commentObj.comments[i]);

										}

									} else {

										$scope.ctrl.children.push(commentObj.comments[i]);

									}

								}

							}

							let threadShow = 5;
							if($scope.ctrl.comments.length<threadShow){
								threadShow = $scope.ctrl.comments.length;
							}
							for(let i=0;i<threadShow;i++) {

								let cond = Object.assign({},params);
								cond.parent = commentObj.comments[i].stub;

								socket.emit('comments',cond);

							}

						} else {

							for(let i=0,c=commentObj.comments.length;i<c;i++) {

								let chL = $scope.ctrl.children.findIndex(x=>x.stub === commentObj.comments[i].stub);
								if(chL<0) {

									$scope.ctrl.children.push(commentObj.comments[i]);
									$scope.$apply();

								} else {

									$scope.ctrl.children[chL] = commentObj.comments[i];
									$scope.$apply();

								}

							}

						}

					}

				} else {

					$rootScope.current.log.push({t:'e',e:'Error in comments',d:commentObj,dt:Math.round((new Date()).getTime()/1000)});

				}

			})
			.catch(e=>{

					$rootScope.current.log.push({t:'e',e:'Error in user-comment-action',d:e,dt:Math.round((new Date()).getTime()/1000)});

			});

	});

	$scope.$on('socket:user-comment',(evt,_d)=>{

		socket.decrypt(_d)
			.then(commentObj=>{

				if(commentObj.ok === true) {

					fn.comments.proc(commentObj);

					var someElement = angular.element(document.getElementById('comment-'+commentObj.row.stub));

					$document.scrollTo(someElement,400);

				} else {

					$rootScope.current.log.push({t:'e',e:'Error in user-comment',d:commentObj,dt:Math.round((new Date()).getTime()/1000)});

				}

			}).catch(e=>{

				$rootScope.current.log.push({t:'e',e:'Error in user-comment',d:e,dt:Math.round((new Date()).getTime()/1000)});

			});

	});

	$scope.$on('socket:user-comment-action',(evt,_d)=>{

		socket.decrypt(_d)
			.then(commentObj=>{

				if(commentObj.ok === true) {

					fn.comments.proc(commentObj);
					var someElement = angular.element(document.getElementById('comment-'+commentObj.row.stub));

					$document.scrollTo(someElement,400);
					
				} else {

					$rootScope.current.log.push({t:'e',e:'Error in user-comment-action',d:commentObj,dt:Math.round((new Date()).getTime()/1000)});

				}

			}).catch(e=>{

				$rootScope.current.log.push({t:'e',e:'Error in user-comment-action',d:e,dt:Math.round((new Date()).getTime()/1000)});

			});

	});
	$scope.$watch('vm.isLoggedIn()',()=>{

		params = fn.params.prepParam();
		socket.emit('items',fn.params.prepParamItem());	
	
	});
	

};