-- --------------------------------------------------------
-- Host:                         tethys
-- Server version:               10.1.23-MariaDB-9+deb9u1 - Raspbian 9.0
-- Server OS:                    debian-linux-gnueabihf
-- HeidiSQL Version:             9.4.0.5125
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;


-- Dumping database structure for euspider
CREATE DATABASE IF NOT EXISTS `euspider` /*!40100 DEFAULT CHARACTER SET utf8 */;
USE `euspider`;

-- Dumping structure for table euspider.news.comment.reported
CREATE TABLE IF NOT EXISTS `news.comment.reported` (
  `user_id` int(255) NOT NULL,
  `comment_id` int(255) NOT NULL,
  `dt` int(10) NOT NULL,
  PRIMARY KEY (`user_id`,`comment_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

-- Data exporting was unselected.
-- Dumping structure for table euspider.news.comments
CREATE TABLE IF NOT EXISTS `news.comments` (
  `id` int(255) NOT NULL AUTO_INCREMENT,
  `item_id` int(255) DEFAULT NULL,
  `parent_id` int(255) DEFAULT NULL,
  `user_id` int(255) NOT NULL,
  `txt` longtext NOT NULL,
  `dt` int(10) NOT NULL,
  `dt_edit` int(10) DEFAULT NULL,
  `isDeleted` tinyint(1) NOT NULL DEFAULT '0',
  `isRemoved` tinyint(1) NOT NULL DEFAULT '0',
  `isRemovedBy` int(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='// select for anon\r\nSELECT \r\n  nc_store.id, nc_store.parent_id, nc_store.comment, nc_store.dt, nc_store.dt_edit,\r\n  nurc.reported, (COALESCE(voteUp.vote,0) - COALESCE(voteDown.vote,0)) AS votes , u.name as user_name, u.id as uid\r\n  FROM (\r\n    SELECT nc.id, nc.parent_id, nc.comment, nc.dt, nc.dt_edit, nc.user_id\r\n    FROM `news.comments` AS nc \r\n    WHERE nc.id IN(\r\n      SELECT id FROM (\r\n        SELECT parent_id, id FROM `news.comments`) base,\r\n        (SELECT @pv := 4) tmp \r\n        WHERE find_in_set(parent_id, @pv) > 0 AND @pv := concat(@pv, ",", id)\r\n      )\r\n	) AS nc_store\r\n	INNER JOIN `users` u ON u.id = nc_store.user_id\r\n	LEFT JOIN (SELECT count(*) AS vote, comment_id FROM `news.user.vote.comments` WHERE dir=1) voteUp ON voteUp.comment_id = nc_store.id \r\n  	LEFT JOIN (SELECT count(*) AS vote, comment_id FROM `news.user.vote.comments` WHERE dir=0) voteDown ON voteDown.comment_id = nc_store.id \r\n  	LEFT JOIN (SELECT COUNT(*) AS reported, comment_id FROM `news.user.report.comments` GROUP BY comment_id) AS nurc ON nurc.comment_id = nc_store.id';

-- Data exporting was unselected.
-- Dumping structure for table euspider.news.item.archive
CREATE TABLE IF NOT EXISTS `news.item.archive` (
  `id` int(255) NOT NULL AUTO_INCREMENT,
  `title` text NOT NULL,
  `link` text NOT NULL,
  `author` varchar(255) DEFAULT NULL,
  `description` longtext,
  `summary` longtext,
  `pubDate` varchar(255) DEFAULT NULL,
  `img` text,
  `_source_id` int(255) NOT NULL,
  `_crc` varchar(128) NOT NULL,
  `_date` char(10) NOT NULL,
  `_dt` char(10) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `_crc` (`_crc`),
  FULLTEXT KEY `title_author` (`title`,`author`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

-- Data exporting was unselected.
-- Dumping structure for table euspider.news.items
CREATE TABLE IF NOT EXISTS `news.items` (
  `id` int(255) NOT NULL AUTO_INCREMENT,
  `title` text NOT NULL,
  `link` text NOT NULL,
  `author` varchar(255) DEFAULT NULL,
  `description` longtext,
  `summary` longtext,
  `pubDate` varchar(255) DEFAULT NULL,
  `img` text,
  `_source_id` int(255) NOT NULL,
  `_crc` varchar(128) NOT NULL,
  `_date` char(10) NOT NULL,
  `_dt` char(10) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `_crc` (`_crc`),
  FULLTEXT KEY `title_author` (`title`,`author`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='// SELECTION\r\nSELECT \r\n  ni.id as stub, ni.title, ni.link, ni.author, ni.content, ni.contentSnippet as snippet, ni.pubDate,ni.img,ni._dt, \r\n  ns.name as source_name, ns.stub as source_stub,\r\n  COALESCE(l.liked,0) AS likes,\r\n  COALESCE(c.commented,0) AS comments\r\nFROM `news.items` as ni \r\nINNER JOIN `news.sources` ns ON ns.id = ni._source_id\r\nLEFT JOIN (SELECT COUNT(*) AS liked, item_id FROM `news.item.likes` GROUP BY item_id) AS l ON l.item_id = ni.id\r\nLEFT JOIN (SELECT COUNT(*) AS commented, item_id FROM `news.item.comments` WHERE isActive = 1 AND isDeleted = 0 GROUP BY item_id) AS c ON c.item_id = ni.id\r\nWHERE ni._dt > ?;\r\n\r\n// CLEANUP\r\nDELETE FROM `news.items` WHERE _dt < 1503447224;\r\n\r\n// ARCHIVE\r\nBEGIN;\r\nINSERT INTO `news.item.archive` \r\n	SELECT * \r\n	FROM `news.items` \r\n	WHERE _dt < 1506127531 \r\n	AND id NOT IN (\r\n		SELECT DISTINCT item_id FROM `news.comments`\r\n	) \r\n	AND id NOT IN (\r\n		SELECT DISTINCT item_id FROM `user.saved.items`\r\n	)\r\n	AND id NOT IN (\r\n		SELECT DISTINCT nc.item_id FROM `news.comments` AS nc INNER JOIN `user.saved.comments` usc ON usc.comment_id = nc.id\r\n	);\r\nDELETE FROM `news.items` \r\n	WHERE _dt < 1506127531 \r\n	AND id NOT IN (\r\n		SELECT DISTINCT item_id FROM `news.comments`\r\n	) \r\n	AND id NOT IN (\r\n		SELECT DISTINCT item_id FROM `user.saved.items`\r\n	)\r\n	AND id NOT IN (\r\n		SELECT DISTINCT nc.item_id FROM `news.comments` AS nc INNER JOIN `user.saved.comments` usc ON usc.comment_id = nc.id\r\n	);\r\nCOMMIT;';

-- Data exporting was unselected.
-- Dumping structure for table euspider.news.sources
CREATE TABLE IF NOT EXISTS `news.sources` (
  `id` int(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(128) DEFAULT NULL,
  `stub` varchar(255) DEFAULT NULL,
  `logo` varchar(128) DEFAULT NULL,
  `isActive` tinyint(1) NOT NULL DEFAULT '1',
  `isOfficial` tinyint(1) NOT NULL DEFAULT '0',
  `url_rss` varchar(255) DEFAULT NULL,
  `url_twitter` varchar(255) DEFAULT NULL,
  `_country` varchar(3) NOT NULL,
  `_dt_update` int(10) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `stub` (`stub`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='// Reset all sources for worker\r\nUPDATE `news.sources` SET _dt_update = null;\r\n\r\n// Get all source data\r\nSELECT \r\nns.name, ns.stub, ns.logo, ns.isPaywall, ns.url_rss, ns.url_twitter, ns._country AS location_country, ns._language AS location_language, _dt_update AS updated,\r\nCOALESCE(count_ni.num,0) AS stat_items,\r\nCOALESCE(count_nss.num,0) AS stat_followers,\r\nCOALESCE(count_nc.num,0) AS stat_comments\r\nFROM `news.sources` AS ns\r\nLEFT JOIN (SELECT COUNT(*) AS num, _source_id FROM `news.items` GROUP BY _source_id) AS count_ni ON count_ni._source_id = ns.id\r\nLEFT JOIN (SELECT COUNT(*) AS num, source_id FROM `news.save.sources` GROUP BY source_id) AS count_nss ON count_nss.source_id = ns.id\r\nLEFT JOIN (SELECT COUNT(*) AS num, sNi._source_id FROM `news.comments` AS sNc INNER JOIN `news.items` sNi ON sNi.id = sNc.item_id GROUP BY sNi._source_id) AS count_nc ON count_nc._source_id = ns.id;';

-- Data exporting was unselected.
-- Dumping structure for table euspider.user.bans
CREATE TABLE IF NOT EXISTS `user.bans` (
  `id` int(255) NOT NULL AUTO_INCREMENT,
  `user_id` int(255) DEFAULT NULL,
  `host` text,
  `reason` text,
  `by_admin` int(255) NOT NULL,
  `dt` int(10) NOT NULL,
  `active` tinyint(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Data exporting was unselected.
-- Dumping structure for table euspider.user.saved.comments
CREATE TABLE IF NOT EXISTS `user.saved.comments` (
  `user_id` int(255) NOT NULL,
  `comment_id` int(255) NOT NULL,
  `active` tinyint(1) NOT NULL DEFAULT '1',
  `dt` int(10) NOT NULL,
  PRIMARY KEY (`user_id`,`comment_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

-- Data exporting was unselected.
-- Dumping structure for table euspider.user.saved.items
CREATE TABLE IF NOT EXISTS `user.saved.items` (
  `user_id` int(255) NOT NULL,
  `item_id` int(255) NOT NULL,
  `active` tinyint(1) NOT NULL DEFAULT '1',
  `dt` int(10) NOT NULL,
  PRIMARY KEY (`user_id`,`item_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

-- Data exporting was unselected.
-- Dumping structure for table euspider.user.saved.sources
CREATE TABLE IF NOT EXISTS `user.saved.sources` (
  `user_id` int(255) NOT NULL,
  `source_id` int(255) NOT NULL,
  `active` tinyint(1) NOT NULL DEFAULT '1',
  `dt` int(10) NOT NULL,
  PRIMARY KEY (`user_id`,`source_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

-- Data exporting was unselected.
-- Dumping structure for table euspider.user.tokens
CREATE TABLE IF NOT EXISTS `user.tokens` (
  `user_id` int(255) NOT NULL,
  `token` varchar(255) NOT NULL,
  `persistent` tinyint(1) NOT NULL DEFAULT '1',
  `dt` int(11) NOT NULL,
  PRIMARY KEY (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Data exporting was unselected.
-- Dumping structure for table euspider.user.voted.comments
CREATE TABLE IF NOT EXISTS `user.voted.comments` (
  `user_id` int(255) NOT NULL,
  `comment_id` int(255) NOT NULL,
  `dir` tinyint(1) NOT NULL,
  `dt` int(10) NOT NULL,
  PRIMARY KEY (`user_id`,`comment_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

-- Data exporting was unselected.
-- Dumping structure for table euspider.user.voted.items
CREATE TABLE IF NOT EXISTS `user.voted.items` (
  `user_id` int(255) NOT NULL,
  `item_id` int(255) NOT NULL,
  `dir` tinyint(1) NOT NULL,
  `dt` int(10) NOT NULL,
  PRIMARY KEY (`user_id`,`item_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Data exporting was unselected.
-- Dumping structure for table euspider.users
CREATE TABLE IF NOT EXISTS `users` (
  `id` int(255) NOT NULL AUTO_INCREMENT,
  `name` varchar(64) NOT NULL,
  `email` varchar(128) DEFAULT NULL,
  `password` varchar(128) NOT NULL,
  `api_id` varchar(128) NOT NULL,
  `api_token` varchar(128) DEFAULT NULL,
  `dt_register` int(10) NOT NULL,
  `dt_deactivate` int(10) DEFAULT NULL,
  `active` int(1) NOT NULL DEFAULT '1',
  `auth` int(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Data exporting was unselected.
/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
